#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "D.h"

TEST_CASE("Test 0")
{
    std::vector<std::vector<int>> g(3);
    g[0].push_back(1);
    g[1].push_back(2);
    REQUIRE(Solve(g, 2, 2, 1, 1, 1, 0) == 2);
}

TEST_CASE("Test 1")
{
    std::vector<std::vector<int>> g(15);
    g[0].push_back(1);
    g[0].push_back(2);
    g[0].push_back(3);
    g[0].push_back(4);
    g[3].push_back(5);
    g[4].push_back(6);
    g[1].push_back(7);
    g[7].push_back(8);
    g[6].push_back(9);
    g[9].push_back(10);
    g[9].push_back(11);
    g[9].push_back(12);
    g[9].push_back(13);
    g[9].push_back(14);
    REQUIRE(Solve(g, 1, 1, 6, 0, 0, 0) == 0);
}

TEST_CASE("Test 2")
{
    std::vector<std::vector<int>> g(24);
    g[0].push_back(1);
    g[0].push_back(2);
    g[1].push_back(3);
    g[2].push_back(4);
    g[2].push_back(5);
    g[1].push_back(6);
    g[4].push_back(7);
    g[6].push_back(8);
    g[3].push_back(9);
    g[3].push_back(10);
    g[5].push_back(11);
    g[8].push_back(12);
    g[8].push_back(13);
    g[9].push_back(14);
    g[11].push_back(15);
    g[15].push_back(16);
    g[14].push_back(17);
    g[12].push_back(18);
    g[13].push_back(19);
    g[15].push_back(20);
    g[19].push_back(21);
    g[20].push_back(22);
    g[18].push_back(23);
    REQUIRE(Solve(g, 10, 12, 21, 11, 14, 13) == 31);
}