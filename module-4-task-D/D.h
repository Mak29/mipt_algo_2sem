#include <iostream>
#include <vector>

class CTree{
public:
    CTree(const std::vector<std::vector<int>>& graph)
    {
        g = graph;
        parent.resize(g.size());
        timeIn.resize(g.size());
        timeOut.resize(g.size());
        dfs(0);
    }
    
    int lca(int a, int b) const
    {
        if (isAncestor(a, b)) {
            return a;
        }
        if (isAncestor(b, a)) {
            return b;
        }
        for (int i = parent[a].size() - 1; i >= 0; --i) {
            if (!isAncestor(parent[a][i], b)) {
                a = parent[a][i];
            }
        }
        return parent[a][0];
    }

private:
    std::vector<std::vector<int>> g;
    std::vector<std::vector<int>> parent;
    std::vector<int> timeIn;
    std::vector<int> timeOut;
    int counter = 0;

    void dfs(int v, int p = 0)
    {
        timeIn[v] = ++counter;
        parent[v].push_back(p);
        for(int i = 1; parent[parent[v][i - 1]].size() >= i && parent[v][i - 1] != 0; ++i) {
            parent[v].push_back(parent[parent[v][i - 1]][i - 1]);
        }
        parent[v].push_back(p);
        for (int x : g[v]) {
            dfs(x, v);
        }
        timeOut[v] = ++counter;
    }
    bool isAncestor(int u, int v) const
    {
        return timeIn[u] <= timeIn[v] && timeOut[u] >= timeOut[v];
    }

};

void update(long long& a, long long& b, long long x, long long y, long long z, long long n)
{
    a = (x * a + y * b + z) % n;
    b = (x * b + y * a + z) % n;
}

long long Solve(const std::vector<std::vector<int>>& g,
    long long m, long long a, long long b, long long x, long long y, long long z)
{
    CTree t(g);
    long long v = t.lca(a % g.size(), b % g.size());
    long long sum = v;
    for (long long i = 1; i < m; ++i) {
        update(a, b, x, y, z, g.size());
        v = t.lca((a + v) % g.size(), b);
        sum += v;
    }
    return sum;
}
/*
int main()
{
    int n = 0;
    int m = 0;
    std::cin >> n >> m;
    std::vector<std::vector<int>> g(n);
    for (int i = 1; i < n; ++i) {
        int v = 0;
        std::cin >> v;
        g[v].push_back(i);
    }
    int a1 = 0;
    int a2 = 0;
    std::cin >> a1 >> a2;
    int x = 0;
    int y = 0;
    int z = 0;
    std::cin >> x >> y >> z;
    std::cout << solve(g, m, a1, a2, x, y, z) << std::endl;
    return 0;
}
*/