#include <climits>
#include <iostream>
#include <vector>

struct CEdge{
    int from = 0;
    int to = 0;
    int cost = 0;
    CEdge(int from = 0, int to = 0, int cost = 0) : from(from), to(to), cost(cost)
    {
    }
};

int FindWay(int numVerticies, std::vector<CEdge>& edges, int from, int to, int k)
{
    std::vector<int> empty(numVerticies, INT_MAX / 3);
    empty[from] = 0;
    std::vector<std::vector<int>> dp(2, empty);
    for (int i = 1; i <= k; ++i) {
        for (int j = 0; j < numVerticies; ++j) {
            dp[0][j] = dp[1][j];
        }
        for (auto edge : edges) {
            if (dp[0][edge.from] + edge.cost < dp[0][edge.to]) {
                dp[1][edge.to] = std::min(dp[1][edge.to], dp[0][edge.from] + edge.cost);
            }
        }
    }
    return (dp[1][to] < INT_MAX / 3 ? dp[1][to] : -1);
}
