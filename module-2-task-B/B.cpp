#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "B.h"

TEST_CASE("zeroTest")
{
    std::vector<CEdge> edges;
    edges.push_back({0, 1, 5});
    edges.push_back({1, 2, 3});
    edges.push_back({2, 0, 7});
    REQUIRE(FindWay(3, edges, 2, 2, 5) == 0);
}
TEST_CASE("positiveTest")
{
    std::vector<CEdge> edges;
    edges.push_back({1 - 1, 2 - 1, 6});
    edges.push_back({5 - 1, 1 - 1, 1});
    edges.push_back({4 - 1, 1 - 1, 9});
    edges.push_back({4 - 1, 5 - 1, 3});
    edges.push_back({4 - 1, 3 - 1, 2});
    edges.push_back({2 - 1, 5 - 1, 7});
    edges.push_back({3 - 1, 5 - 1, 1});
    REQUIRE(FindWay(5, edges, 4 - 1, 1 - 1, 2) == 4);
}
TEST_CASE("negativeTest")
{
    std::vector<CEdge> edges;
    edges.push_back({1 - 1, 2 - 1, 4});
    edges.push_back({2 - 1, 3 - 1, 5});
    edges.push_back({3 - 1, 1 - 1, 6});
    REQUIRE(FindWay(3, edges, 1 - 1, 3 - 1, 1) == -1);
}