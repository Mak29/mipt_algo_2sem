#include <iostream>
#include <string>
#include <vector>

template<typename T>
T min(T x, T y)
{
	return ( x < y ? x : y );
}

class CBigInteger {
public:
	bool sign = true;

	CBigInteger abs() const
	{
		CBigInteger c = *this;
		c.sign = true;
		return c;
	}

	CBigInteger() = default;
	CBigInteger(const CBigInteger& x) : digits(x.digits)
	{
		sign = x.sign;
	}
	CBigInteger(const long long& x)
	{
		digits.clear();
		sign = ( x >= 0 ? true : false );
		long long cx = ( x >= 0 ? x : -x );
		while (cx > 0) {
			digits.push_back(cx % mod);
			cx /= mod;
		}
		digits.push_back(cx / mod);
		deleteNull();
	}
	~CBigInteger() = default;

	CBigInteger& operator=(const CBigInteger& x)
	{
		digits = x.digits;
		sign = x.sign;
		return *this;
	}

	explicit operator bool() const
	{
		return static_cast<bool>( digits.size() );
	}

	friend std::istream& operator>>(std::istream&, CBigInteger&);
	friend std::ostream& operator<<(std::ostream&, const CBigInteger&);

	friend CBigInteger operator-(const CBigInteger&);

	CBigInteger& operator++()
	{
		if (!digits.size()) {
			digits.push_back(1);
			return *this;
		}
		if (!sign) {
			sign = true;
			operator--();
			sign = !digits.size();
			return *this;
		}
		++digits[0];
		for (size_t i = 0; digits[i] >= mod; ++i) {
			digits[i] -= mod;
			if (i == digits.size() - 1) {
				digits.push_back(1);
			} else {
				++digits[i + 1];
			}
		}
		deleteNull();
		return *this;
	}
	CBigInteger& operator--()
	{
		if (!digits.size()) {
			sign = false;
			digits.push_back(1);
			return *this;
		}
		if (!sign) {
			sign = true;
			operator++();
			sign = false;
			return *this;
		}
		--digits[0];
		for (size_t i = 0; digits[i] < 0; ++i) {
			digits[i] += mod;
			--digits[i + 1];
		}
		deleteNull();
		return *this;
	}
	CBigInteger operator++(int)
	{
		CBigInteger tmp(*this);
		operator++();
		return tmp;
	}
	CBigInteger operator--(int)
	{
		CBigInteger tmp(*this);
		operator--();
		return tmp;
	}

	CBigInteger& operator+=(const CBigInteger& x)
	{
		if (!x) {
			return *this;
		}
		if (sign ^ x.sign) {
			*this -= ( -x );
			return *this;
		}
		add(x);
		return *this;
	}
	CBigInteger& operator-=(const CBigInteger& x)
	{
		if (!x) {
			return *this;
		}
		if (sign ^ x.sign) {
			add(-x);
			return *this;
		}
		CBigInteger c = abs();
		CBigInteger cx = x.abs();

		if (c < cx) {
			std::swap(c, cx);
			sign = !sign;
		}
		if (c != cx) {
			c.subtract(cx);
			digits = c.digits;
		} else {
			digits.clear();
			sign = true;
		}
		return *this;
	}
	CBigInteger& operator*=(const CBigInteger& x)
	{
		if (!( *this )) {
			return *this;
		}
		if (!x) {
			digits.clear();
			sign = true;
			return *this;
		}
		bool csign = sign;
		sign = true;
		CBigInteger cx = x;
		cx.sign = true;
		*this = multiply(cx);
		sign = !csign ^ x.sign;
		return *this;
	}
	CBigInteger& operator/=(const CBigInteger& x)
	{
		if (!( *this )) {
			return *this;
		}
		bool csign = sign;
		sign = true;
		CBigInteger cx = x;
		cx.sign = true;
		*this = divide(cx);
		sign = !csign ^ x.sign;
		return *this;
	}
	CBigInteger& operator%=(const CBigInteger& x)
	{
		if (!( *this )) {
			return *this;
		}
		bool csign = sign;
		sign = true;
		CBigInteger cx(x);
		cx.sign = true;
		divide(cx);
		sign = !csign ^ x.sign;
		return *this;
	}

	friend CBigInteger operator+(const CBigInteger&, const CBigInteger&);
	friend CBigInteger operator-(const CBigInteger&, const CBigInteger&);
	friend CBigInteger operator*(const CBigInteger&, const CBigInteger&);
	friend CBigInteger operator/(const CBigInteger&, const CBigInteger&);
	friend CBigInteger operator%(const CBigInteger&, const CBigInteger&);

	friend bool operator<=(const CBigInteger&, const CBigInteger&);
	friend bool operator>=(const CBigInteger&, const CBigInteger&);
	friend bool operator==(const CBigInteger&, const CBigInteger&);
	friend bool operator!=(const CBigInteger&, const CBigInteger&);
	friend bool operator<(const CBigInteger&, const CBigInteger&);
	friend bool operator>(const CBigInteger&, const CBigInteger&);

	std::string toString() const
	{
		if (!digits.size()) {
			return "0";
		}
		std::string out = ( sign ? "" : "-" );
		out += std::to_string(digits.back());
		for (int i = static_cast<int>(digits.size()) - 2; i >= 0; --i) {
			std::string block = std::to_string(digits[i]);
			int diff = static_cast<int>(blockSize) - static_cast<int>(block.size());
			out += ( ( diff > 0 ? std::string(diff, '0') : "" ) + block );
		}
		return out;
	}

private:
	// количество десятичных цифр в разряде
	static const size_t blockSize = 9;

	// основание системы счисления
	static const long long mod = 1000000000;

	// цифры числа
	std::vector<int> digits;

	// добавление к данном числу другого, сдвинутого на shift разрядов влево
	// слагаемые должны быть одного знака
	void add(const CBigInteger& other, size_t shift = 0)
	{
		if (!other) {
			return;
		}
		if (digits.size() <= shift) {
			digits.resize(shift + 1, 0);
		}
		int add = other.digits[0];
		for (size_t i = 0; i < other.digits.size() || add > 0; ++i) {
			if (i + shift >= digits.size()) {
				digits.push_back(add);
			} else {
				digits[shift + i] += add;
			}
			add = digits[shift + i] / mod + ( i + 1 < other.digits.size() ? other.digits[i + 1] : 0 );
			digits[shift + i] %= mod;
		}
	}

	// вычитаемое должно быть по модулю меньше уменьшаемого
	void subtract(const CBigInteger& other)
	{
		bool takeUp = false;
		for (size_t i = 0; i < other.digits.size() || takeUp; ++i) {
			int subtract = ( i < other.digits.size() ? other.digits[i] : 0 );
			takeUp = digits[i] < subtract;
			if (!takeUp) {
				digits[i] -= subtract;
			} else {
				digits[i] += ( mod - subtract );
				--digits[i + 1];
			}
		}
		deleteNull();
	}

	CBigInteger(std::vector<int>::iterator left, std::vector<int>::iterator right)
	{
		sign = true;
		digits.clear();
		for (auto i = left; i != right; ++i) {
			digits.push_back(*i);
		}
		deleteNull();
	}

	// умножение алгоритмом Карацубы
	CBigInteger multiply(CBigInteger& x)
	{
		if (!*this || !x) {
			return 0;
		}
		if (digits.size() == 1 && x.digits.size() == 1) {
			long long a = digits[0];
			long long b = x.digits[0];
			return a * b;
		}
		if (static_cast<int>(digits.size()) & 1) {
			digits.push_back(0);
		}
		if (static_cast<int>(x.digits.size()) & 1) {
			x.digits.push_back(0);
		}
		if (digits.size() < x.digits.size()) {
			fitSize(x);
		} else if (digits.size() > x.digits.size()) {
			x.fitSize(*this);
		}

		int n = static_cast<int>(digits.size()) / 2;
		CBigInteger r1(digits.begin(), digits.begin() + n);
		CBigInteger l1(digits.begin() + n, digits.end());
		CBigInteger r2(x.digits.begin(), x.digits.begin() + n);
		CBigInteger l2(x.digits.begin() + n, x.digits.end());
		CBigInteger out = ( r1 * r2 );
		CBigInteger part1 = ( l1 * l2 );
		CBigInteger part2 = ( ( l1 + r1 ) * ( l2 + r2 ) );
		part2 -= part1 + out;

		out.add(part2, static_cast<size_t>( n ));
		out.add(part1, 2 * static_cast<size_t>( n ));

		deleteNull();
		x.deleteNull();
		out.deleteNull();
		return out;
	}

	// деление столбиком
	CBigInteger divide(CBigInteger& x)
	{
		CBigInteger out;

		out.sign = !sign ^ x.sign;
		int shift = static_cast<int>(digits.size()) - static_cast<int>(x.digits.size());
		for (; shift >= 0; --shift) {
			if (shift >= static_cast<int>(digits.size())) {
				out.digits.push_back(0);
				continue;
			}
			CBigInteger slice(digits.begin() + shift, digits.end()); //
			int left = 0;
			int right = mod;
			while (right - left > 1) {
				int middle = left + ( right - left ) / 2;
				if (( middle * x ) <= slice) {
					left = middle;
				} else {
					right = middle;
				}
			}
			out.digits.push_back(left);
			slice -= ( static_cast<CBigInteger>( left )* x );

			for (unsigned i = 0; i < digits.size() - shift; ++i) {
				if (i < slice.digits.size()) {
					digits[shift + i] = slice.digits[i];
				} else {
					digits[shift + i] = 0;
				}
			}
			deleteNull();
		}

		for (size_t i = 0; i < out.digits.size() / 2; ++i) {
			std::swap(out.digits[i], out.digits[out.digits.size() - i - 1]);
		}
		out.deleteNull();
		return out;
	}

	void checkOverflow(size_t i)
	{
		if (digits[i] >= mod) {
			if (i == digits.size() - 1) {
				digits.push_back(digits[i] / mod);
			} else {
				digits[i + 1] += digits[i] / mod;
			}
			digits[i] %= mod;
			checkOverflow(i + 1);
		}
	}

	void fitSize(const CBigInteger& other)
	{
		if (digits.size() < other.digits.size()) {
			digits.resize(other.digits.size(), 0);
		}
	}
	void deleteNull()
	{
		for (int i = static_cast<int>(digits.size()) - 1; i >= 0; --i) {
			if (digits[static_cast<size_t>( i )] == 0) {
				digits.pop_back();
			} else {
				return;
			}
		}
		sign = true;
	}
};

std::istream& operator>>(std::istream& in, CBigInteger& number)
{
	number.digits.clear();
	std::string s;
	in >> s;
	int end = 0;
	if (s[0] == '-') {
		end = 1;
		number.sign = false;
	} else {
		number.sign = true;
	}
	for (int i = static_cast<int>(s.size()) - 1; i >= end; ) {
		int x = 0;
		int d = 1;
		for (size_t j = 0; i >= end && j < number.blockSize; --i, ++j) {
			x += d * ( s[i] - '0' );
			d *= 10;
		}
		number.digits.push_back(x);
	}
	number.deleteNull();
	if (!number.digits.size()) {
		number.sign = true;
	}
	return in;
}

std::ostream& operator<<(std::ostream& out, const CBigInteger& number)
{
	out << number.toString();
	return out;
}

CBigInteger operator-(const CBigInteger& a)
{
	if (!a) {
		return a;
	}
	CBigInteger b = a;
	b.sign = !b.sign;
	return b;
}

CBigInteger operator+(const CBigInteger& a, const CBigInteger& b)
{
	CBigInteger c = a;
	return c += b;
}

CBigInteger operator-(const CBigInteger& a, const CBigInteger& b)
{
	CBigInteger c = a;
	return c -= b;
}

CBigInteger operator*(const CBigInteger& a, const CBigInteger& b)
{
	CBigInteger c = a;
	return c *= b;
}

CBigInteger operator/(const CBigInteger& a, const CBigInteger& b)
{
	CBigInteger c(a);
	c /= b;
	return c;
}

CBigInteger operator%(const CBigInteger& a, const CBigInteger& b)
{
	CBigInteger c(a);
	c %= b;
	return c;
}

bool operator<=(const CBigInteger& a, const CBigInteger& b)
{
	if (!a) {
		if (!b) {
			return true;
		}
		return b.sign;
	}
	if (a.sign ^ b.sign) {
		return b.sign;
	}
	if (!a.sign && !b.sign) {
		return ( -b ) <= ( -a );
	}
	if (a.digits.size() != b.digits.size()) {
		return a.digits.size() <= b.digits.size();
	}
	for (int i = static_cast<int>(a.digits.size()) - 1; i >= 0; --i) {
		if (a.digits[i] != b.digits[i]) {
			return a.digits[i] <= b.digits[i];
		}
	}
	return true;
}
bool operator>=(const CBigInteger& a, const CBigInteger& b)
{
	return b <= a;
}
bool operator==(const CBigInteger& a, const CBigInteger& b)
{
	return ( a <= b ) && ( b <= a );
}
bool operator!=(const CBigInteger& a, const CBigInteger& b)
{
	return !( a == b );
}
bool operator<(const CBigInteger& a, const CBigInteger& b)
{
	return a <= b && !( b <= a );
}
bool operator>(const CBigInteger& a, const CBigInteger& b)
{
	return b < a;
}

class CRational {
public:
	CRational() = default;
	CRational(long long x) : numerator(x), denominator(1)
	{
	}
	CRational(const CBigInteger& numerator, const CBigInteger& denominator = 1)
		: numerator(numerator), denominator(denominator)
	{
		reduction();
	}

	explicit operator double()
	{
		CBigInteger fraction = numerator;
		for (int i = 0; i < doubleExponent; ++i) {
			fraction *= 10;
		}
		fraction /= denominator;
		std::string fractionStr = fraction.toString();
		double out = 1.;
		if (!fraction.sign) {
			out = -1.;
			fractionStr.erase(0, 1);
		}
		std::string s = fractionStr.substr(0, doublePrecision) + 'e';
		int exp = static_cast<int>(fractionStr.size()) - doublePrecision - doubleExponent;
		s += std::to_string(exp);
		out *= stod(s);
		return out;
	}

	friend CRational operator-(const CRational&);

	CRational& operator+=(const CRational& x)
	{
		numerator = ( numerator * x.denominator + denominator * x.numerator );
		denominator *= x.denominator;
		reduction();
		return *this;
	}
	CRational& operator-=(const CRational& x)
	{
		numerator = ( numerator * x.denominator - denominator * x.numerator );
		denominator *= x.denominator;
		reduction();
		return *this;
	}
	CRational& operator*=(const CRational& x)
	{
		numerator *= x.numerator;
		denominator *= x.denominator;
		reduction();
		return *this;
	}
	CRational& operator/=(const CRational& x)
	{
		numerator *= x.denominator;
		denominator *= x.numerator;
		reduction();
		return *this;
	}

	friend CRational operator+(const CRational&, const CRational&);
	friend CRational operator-(const CRational&, const CRational&);
	friend CRational operator*(const CRational&, const CRational&);
	friend CRational operator/(const CRational&, const CRational&);

	friend bool operator<=(const CRational&, const CRational&);
	friend bool operator>=(const CRational&, const CRational&);
	friend bool operator==(const CRational&, const CRational&);
	friend bool operator!=(const CRational&, const CRational&);
	friend bool operator<(const CRational&, const CRational&);
	friend bool operator>(const CRational&, const CRational&);

	std::string toString()
	{
		if (denominator == 1) {
			return numerator.toString();
		} else {
			return numerator.toString() + '/' + denominator.toString();
		}
	}
	std::string asDecimal(size_t precision = 0)
	{
		if (!precision) {
			return ( numerator / denominator ).toString();
		}
		std::string integer = ( numerator.sign ? "" : "-" ) + ( numerator.abs() / denominator ).toString();
		CBigInteger copy = numerator.abs();
		for (size_t i = 0; i < precision; ++i) {
			copy *= 10;
		}

		std::string fractional = ( copy / denominator ).toString();
		if (fractional.size() < precision) {
			fractional = std::string(precision - fractional.size(), '0') + fractional;
		}
		return integer + '.' + fractional.substr(fractional.size() - precision, precision);
	}

private:
	CBigInteger numerator;
	CBigInteger denominator;
	static const int doublePrecision = 17;
	static const int doubleExponent = 325;

	void reduction()
	{
		CBigInteger absNumerator(numerator);
		absNumerator.sign = true;
		CBigInteger absDenominator(denominator);
		absDenominator.sign = true;
		CBigInteger gcd = findGCD(absNumerator, absDenominator);
		gcd.sign = denominator.sign;
		numerator /= gcd;
		denominator /= gcd;
	}
	CBigInteger findGCD(CBigInteger& one, CBigInteger& two)
	{
		if (two) {
			return findGCD(two, one %= two);
		} else {
			return one;
		}
	}

	bool sign()
	{
		return numerator.sign;
	}
};

CRational operator-(const CRational& a)
{
	return CRational(-a.numerator, a.denominator);
}

CRational operator+(const CRational& a, const CRational& b)
{
	return CRational(a) += b;
}

CRational operator-(const CRational& a, const CRational& b)
{
	return CRational(a) -= b;
}

CRational operator*(const CRational& a, const CRational& b)
{
	return CRational(a) *= b;
}

CRational operator/(const CRational& a, const CRational& b)
{
	return CRational(a) /= b;
}

bool operator<=(const CRational& a, const CRational& b)
{
	return ( b - a ).sign();
}
bool operator>=(const CRational& a, const CRational& b)
{
	return b <= a;
}
bool operator==(const CRational& a, const CRational& b)
{
	return ( a.numerator * b.denominator ) == ( a.denominator * b.numerator );
}
bool operator!=(const CRational& a, const CRational& b)
{
	return !( a == b );
}
bool operator<(const CRational& a, const CRational& b)
{
	return a <= b && a != b;
}
bool operator>(const CRational& a, const CRational& b)
{
	return b <= a && a != b;
}
