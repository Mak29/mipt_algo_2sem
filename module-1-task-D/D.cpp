#include <algorithm>
#include <cassert>
#include <iostream>
#include <set>
#include <queue>
#include <vector>

struct CSegment {
	// множество всех вершин сегмента
	std::set<int> vertex;

	// количество граней, в которые сегмент входит
	int faces = 0;

	// номер любой грани, куда входит сегмент
	int numFace = 0;

	// множество контактных вершин сегмента
	std::set<int> contact;

	// массив флагов: для каждой грани храним, укладывается ли в неё сегмент
	std::vector<bool> isInFace;

	CSegment() = default;
};

bool operator<( const CSegment& u, const CSegment& v )
{
	return u.faces > v.faces;
}

struct CFace {
	std::set<int> sorted;
	std::vector<int> ordered;

	void pushVertex( int v )
	{
		sorted.insert( v );
		ordered.push_back( v );
	}
};

class CComponent {
public:
	CComponent( size_t size ) : size( size ) {
		graph.resize( size );
		gplane.resize( size );
		twoVertexSegments.resize( size );
		for( int i = 0; i < size; ++i ) {
			gplane[i].resize( size, false );
			twoVertexSegments[i].resize( size, false );
		}
	}
	~CComponent() = default;

	void PushEdge( int x, int y )
	{
		int m = std::max( x, y );
		if( x < size )
			graph[x].push_back( y );
		graph[y].push_back( x );
	}
	bool IsPlanar()
	{
		findCycle();
		faces.push_back( { plane, current } );
		faces.push_back( { plane, current } );
		current.clear();
		for( int i = 0; i < graph.size(); ++i ) {
			if( plane.find( i ) == plane.end() ) {
				segmentVertexes.insert( i );
			}
		}
		findSegments();
		if( !segments.size() ) {
			return true;
		}
		// будем хранить сегмент, который укладывается в минимальное число граней, последним
		std::swap( segments[minSegment], segments[segments.size() - 1] );

		minSegment = 0;
		while( !segments.empty() && segments.back().faces != 0 ) { // выделить второе условие в отдельную функцию
			CSegment c = segments.back();
			firstChanged = segments.size() - 1;
			findChaine( c );
			divideFace();
			updateSegments();
			cutSegment();
			minSegment = 0;
			for( int i = 0; i < segments.size(); ++i ) {
				if( segments[i].faces < segments[minSegment].faces ) {
					minSegment = i;
				}
			}
			if( !segments.empty() ) {
				std::swap( segments[minSegment], segments[segments.size() - 1] );
			}
		}
		return ( segments.empty() ? true : false );
	}

private:
	// Про каждое ребро храним, укладывали ли мы его ранее
	std::vector< std::vector<bool> > gplane;

	// список смежностей
	std::vector< std::vector<int> > graph;

	// запоминаем про каждое ребро, было ли оно сегментом, который состоит из одного ребра
	std::vector< std::vector<bool> > twoVertexSegments;

	// массив граней
	std::vector<CFace> faces;

	// массив сегментов
	std::vector<CSegment> segments;

	// для каждой вершины храним флаг, находится ли она в цепи
	std::vector<bool> inChaine;

	// в рызличных обходах для каждой вершины храним, были ли мы в этой вершине в данном обходе
	std::vector<bool> openedVertex;

	// массив вершин цепи
	std::vector<int> chain;

	// массив, в котором при старте алгоритма хранится цикл,
	// а затем хранятся вершины грани, которую разбиваем вместе с цепью
	std::vector<int> current;

	// массив, в котором хранится, для каждой вершины, была ли она в цикле, и если была,
	// то какой по счёту при обходе
	std::vector<int> numberInCycle;

	// множество уложенных вершин
	std::set<int> plane;

	// множество вершин, которые лежат в каком-нибудь сегменте
	std::set<int> segmentVertexes;

	// количество вершин в графе
	size_t size;

	// индекс сегмента, который укладывается в минимальное число граней
	int minSegment = 0;

	// номер первого нового сегмента, на итерации
	int firstChanged = 0;

	void divideFace()
	{
		int numFace = segments.back().numFace;
		CFace divFace;
		current.clear();
		faces.push_back( divFace );
		std::swap( divFace, faces[numFace] );
		int i = 0;
		for( int i = 0; i < divFace.ordered.size(); ++i ) {
			if( divFace.ordered[i] == chain[0] ) {
				break;
			}
			if( divFace.ordered[i] == chain.back() ) {
				std::reverse( chain.begin(), chain.end() );
				break;
			}
		}
		for( ; chain[0] != divFace.ordered[i]; ++i ) {
			faces.back().pushVertex( divFace.ordered[i] );
			current.push_back( divFace.ordered[i] );
		}
		for( ; chain.back() != divFace.ordered[i]; ++i ) {
			faces[numFace].pushVertex( divFace.ordered[i] );
			current.push_back( divFace.ordered[i] );
		}
		for( int i = 0; i < chain.size() - 1; ++i ) {
			faces[numFace].pushVertex( chain[chain.size() - i - 1] );
			faces.back().pushVertex( chain[i] );
			if( i != 0 ) {
				current.push_back( chain[i] );
			}
			plane.insert( chain[i] );
			gplane[chain[i]][chain[i + 1]] = true;
			gplane[chain[i + 1]][chain[i]] = true;
		}
		for( ; i < divFace.ordered.size(); ++i ) {
			faces.back().pushVertex( divFace.ordered[i] );
			current.push_back( divFace.ordered[i] );
		}
		for( int i = 0; i < segments.size(); ++i ) {
			segments[i].isInFace.resize( faces.size() );
		}
	}
	void updateSegments()
	{
		int numFace = segments.back().numFace;
		int i = 0;
		for( ; i < firstChanged; ++i ) {
			segments[i].faces += isSegmentInFace( segments[i], faces.size() - 1 );
			if( segments[i].isInFace[numFace] ) {
				--segments[i].faces;
			}
			segments[i].faces += isSegmentInFace( segments[i], numFace );
			if( !segments[i].isInFace[segments[i].numFace] ) {
				for( int j = 0; j < faces.size(); ++j ) {
					if( segments[i].isInFace[j] ) {
						segments[i].numFace = j;
					}
				}
			}
		}
	}
	void writeOpened( CFace& face )
	{
		for( int i = 0; i < face.ordered.size(); ++i ) {
			openedVertex[face.ordered[i]] = true;
		}
	}
	void cutSegment()
	{
		openedVertex.clear();
		openedVertex.resize( size, false );
		writeOpened( faces[segments.back().numFace] );
		writeOpened( faces.back() );

		CSegment& s = segments.back();
		for( auto i = s.vertex.begin(); i != s.vertex.end(); ++i ) {
			if( s.contact.find( *i ) == s.contact.end() ) {
				segmentVertexes.insert( *i );
			}
		}
		segments.pop_back();
		for( auto i = current.begin(); i != current.end(); ++i ) {
			for( int j = 0; j < graph[*i].size(); ++j ) {
				findOneSegment( *i, graph[*i][j] );
			}
		}
	}
	bool isInPlane( int v )
	{
		return ( plane.find( v ) != plane.end() );
	}
	bool isNotFirstInChain( int v )
	{
		return ( v != chain[0] );
	}
	bool dfsFindChaine( int v ) // v - вершина, от которой запускается dfs
	{
		inChaine[v] = true;
		chain.push_back( v );
		if( isInPlane( v ) && isNotFirstInChain( v ) ) {
			return true;
		}
		for( int i = 0; i < graph[v].size(); ++i ) {
			if( segments.back().vertex.find( graph[v][i] ) != segments.back().vertex.end()
			 	&& !gplane[v][graph[v][i]] && !inChaine[graph[v][i]] && dfsFindChaine( graph[v][i] ) ) {
					return true;
			}
		}
		chain.pop_back();
		return false;
	}
	void findChaine( CSegment& s )
	{
		inChaine.clear();
		chain.clear();
		inChaine.resize( size );
		assert( dfsFindChaine( *( s.contact.begin() ) ) );
	}
	bool dfsFindCycle( int v, int parent )
	{
		openedVertex[v] = true;
		current.push_back( v );
		numberInCycle[v] = current.size() - 1;
		for( int i = 0; i < graph[v].size(); ++i ) {
			if( !openedVertex[graph[v][i]] ) {
				bool f = dfsFindCycle( graph[v][i], v );
				if( f ) {
					return true;
				}
			} else if( graph[v][i] != parent ) {
				current.push_back( graph[v][i] );
				return true;
			}
		}
		current.pop_back();
		numberInCycle[v] = -1;
		return false;
	}
	void findCycle()
	{
		openedVertex.resize( size );
		numberInCycle.resize( size, -1 );
		dfsFindCycle( 0, 0 );
		int begin = numberInCycle[current.back()]; 
		for( int i = begin; i < current.size() - 1; ++i ) {
			current[i - begin] = current[i];
		}
		current.resize( current.size() - begin - 1 );
		openedVertex.clear();
		openedVertex.resize( size, false );
		for( int i = 0; i < current.size() - 1; ++i ) {
			openedVertex[current[i]] = true;
			plane.insert( current[i] );
			gplane[current[i]][current[i + 1]] = true;
			gplane[current[i + 1]][current[i]] = true;
		}
		plane.insert( current.back() );
		openedVertex[current.back()] = true;
		gplane[current.front()][current.back()] = true;
		gplane[current.back()][current.front()] = true;
	}
	bool isSegmentInFace( CSegment& s, int i )
	{
		bool isInFace = true;
		for( auto j = s.contact.begin(); j != s.contact.end(); ++j ) {
			if( faces[i].sorted.find( *j ) == faces[i].sorted.end() ) {
				isInFace = false;
				break;
			}
		}
		if( isInFace ) {
			s.numFace = i;
		}
		s.isInFace[i] = isInFace;
		return isInFace;
	}
	void countFacesForSegment( CSegment& s )
	{
		s.faces = 0;
		for( int i = 0; i < faces.size(); ++i ) {
			s.faces += isSegmentInFace( s, i );
		}
	}
	bool isOpenedSegment( int first, int start )
	{
		if( gplane[first][start] ||  twoVertexSegments[first][start] ) {
			return true;
		}
		if( segmentVertexes.find( start ) == segmentVertexes.end() && plane.find( start ) == plane.end() ) {
			return true;
		}
		return false;
	}
	void pushInSegment( std::queue<int>& que, int v )
	{
		que.push( v );
		openedVertex[v] = true;
		if( segmentVertexes.find( v ) != segmentVertexes.end() ) {
			segmentVertexes.erase( v );
		}
	}
	void init( CSegment& s, int first, int start )
	{
		s.vertex.insert( first );
		s.vertex.insert( start );
		
		s.contact.insert( first );
		s.isInFace.clear();
		s.isInFace.resize( faces.size(), false );
		if( plane.find( start ) != plane.end() ) {
			s.contact.insert( start );
			twoVertexSegments[first][start] = true;
			twoVertexSegments[start][first] = true;
		}
	}
	void findOneSegment( int first, int start )
	{
		if( isOpenedSegment( first, start ) ) {
			return;
		}
		CSegment looking;
		init( looking, first, start );
		std::queue<int> que;
		if( plane.find( start ) == plane.end() ) {
			pushInSegment( que, start );
		}
		while( !que.empty() ) {
			int v = que.front();
			que.pop();
			for( int i = 0; i < graph[v].size(); ++i ) {
				if( !openedVertex[graph[v][i]] || plane.find( graph[v][i] ) != plane.end() ) {
					if( plane.find( graph[v][i] ) == plane.end() ) {
						pushInSegment( que, graph[v][i] );
					} else {
						looking.contact.insert( graph[v][i] );
					}
					looking.vertex.insert( graph[v][i] );
				}
			}
		}
		looking.numFace = 0;
		countFacesForSegment( looking );
		segments.push_back( looking );
		if( segments.back().faces < segments[minSegment].faces ) {
			minSegment = segments.size() - 1;
		}
	}
	void findSegments()
	{
		for( auto i = plane.begin(); i != plane.end(); ++i ) {
			for( int j = 0; j < graph[*i].size(); ++j ) {
				findOneSegment( *i, graph[*i][j] );
			}
		}
	}
};

class CGraph {
public:
	CGraph( size_t size ) : size( size )
	{
		graph.resize( size );
	}
	~CGraph() = default;

	void PushEdge( int x, int y )
	{
		graph[x].insert( y );
		graph[y].insert( x );
	}
	bool IsPlanar()
	{
		delBridges();
		delExtraVertexes();
		openedVertexes.clear();
		openedVertexes.resize( graph.size(), false );
		for( int i = 0; i < graph.size(); ++i ) {
			if( !openedVertexes[i] && graph[i].size() ) {
				CComponent c( size );
				dfsConstructComponent( i, c );
				if( !c.IsPlanar() ) {
					return false;
				}
			}
		}
		return true;
	}

private:
	// списки смежности
	std::vector< std::set<int> > graph;

	// массив мостов
	std::vector< std::pair<int, int> > bridges;

	// времена входа в вершину
	std::vector<int> inputTime;

	// минимальное время входа среди всех вершин, до которых можно дойти из поддерева,
	// идя не более чем по одному обратному ребру
	std::vector<int> minReachableTime;

	// массив флагов, были ли мы в вершине во время обхода
	std::vector<bool> openedVertexes;

	// количество вершин в графе
	size_t size;

	// время входа
	int in = 0;

	void dfsConstructComponent( int v, CComponent& c )
	{ // в этом обходе в глубину кидаем всю компоненту связности в CComponent
		openedVertexes[v] = true;
		for( auto i = graph[v].begin(); i != graph[v].end(); ++i ) {
			c.PushEdge( v, *i );
			if( !openedVertexes[*i] ) {
				dfsConstructComponent( *i, c );
			}
		}
	}
	void dfsFindBridges( int v, int p ) // обход графа, при котором ищем мосты
	{ // v - стратовая вершина, p - откуда в неё пришли
		openedVertexes[v] = true;
		inputTime[v] = in++;
		minReachableTime[v] = inputTime[v];
		for( auto i = graph[v].begin(); i != graph[v].end(); ++i ) {
			if( !openedVertexes[*i] ) { 
				dfsFindBridges( *i, v );
				minReachableTime[v] = std::min( minReachableTime[v], minReachableTime[*i] );
				if( minReachableTime[*i] > inputTime[v] ) {
					bridges.push_back( { v, *i } );
				}
			} else if( p != *i ) { // проверка того, не смотрим ли мы на ребро, по которому пришли
				minReachableTime[v] = std::min( minReachableTime[v], inputTime[*i] );
			}
		}
	}
	void delBridges()
	{
		openedVertexes.resize( size, false );
		inputTime.resize( size, 0 );
		minReachableTime.resize( size, 0 );
		for( size_t i = 0; i < graph.size(); ++i ) {
			if( !openedVertexes[i] ) {
				dfsFindBridges( i, i );
			}
		}
		for( auto i = bridges.begin(); i != bridges.end(); ++i ) {
			graph[i->first].erase( i->second );
			graph[i->second].erase( i->first );
		}
	}
	bool isDangling( int v )
	{
		return ( graph[v].size() == 1 );
	}
	void delExtraVertexes()
	{
		for( size_t i = 0; i < graph.size(); ++i ) {
			if( isDangling( i ) ) {
				graph[*( graph[i].begin() )].erase( i );
				graph[i].clear();
			}
		}
	}
};

int main()
{
	size_t numVertices = 0;
	size_t numEdges = 0;
	std::cin >> numVertices >> numEdges;
	CGraph g( numVertices );
	for( int i = 0; i < numEdges; ++i ) {
		int x = 0;
		int y = 0;
		std::cin >> x >> y;
		if( x != y ) {
			g.PushEdge( x, y );
		}
	}
	
	std::cout << ( g.IsPlanar() ? "YES" : "NO" );
	return 0;
}