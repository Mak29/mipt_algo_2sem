#include <iostream>
#include <vector>

long long Prima(const std::vector<std::vector<std::pair<int, int>>>& g)
{
    std::vector<int> dist(g.size(), 100005);
    std::vector<bool> used(g.size(), false);
    dist[0] = 0;
    long long sum = 0;
    for (int i = 0; i < g.size(); ++i) {
        int minVertex = 0;
        for (int j = 0; j < dist.size(); ++j) {
            if (used[minVertex] || !used[j] && dist[j] < dist[minVertex]) {
                minVertex = j;
            }
        }
        sum += dist[minVertex];
        used[minVertex] = true;
        for (auto v : g[minVertex]) {
            if (v.second < dist[v.first]) {
                dist[v.first] = v.second;
            }
        }
    }
    return sum;
}
/*
int main()
{
    int numVertices = 0;
    int numEdges = 0;
    std::cin >> numVertices >> numEdges;
    std::vector<std::vector<std::pair<int, int>>> g(numVertices);
    for (int i = 0; i < numEdges; ++i) {
        int from = 0;
        int to = 0;
        int weight = 0;
        std::cin >> from >> to >> weight;
        --from;
        --to;
        g[from].push_back({to, weight});
        g[to].push_back({from, weight});
    }
    std::cout << Prima(g) << std::endl;
    return 0;
}
*/