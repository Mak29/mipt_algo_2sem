#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "a.h"

TEST_CASE("Test 0")
{
    int n = 1;
    int m = 0;
    std::vector<std::vector<std::pair<int, int>>> g(n);
    REQUIRE(Prima(g) == 0);
}

TEST_CASE("Test 1")
{
    int n = 2;
    int m = 1;
    std::vector<std::vector<std::pair<int, int>>> g(n);
    g[1 - 1].push_back({2 - 1, 10986});
    g[2 - 1].push_back({1 - 1, 10986});
    REQUIRE(Prima(g) == 10986);
}

TEST_CASE("Test 2")
{
    int n = 4;
    int m = 6;
    std::vector<std::vector<std::pair<int, int>>> g(4);
    g[0].push_back({1, 6});
    g[0].push_back({2, 3});
    g[0].push_back({3, 6});
    g[1].push_back({2, 5});
    g[1].push_back({3, 4});
    g[2].push_back({3, 5});
    g[1].push_back({0, 6});
    g[2].push_back({0, 3});
    g[3].push_back({0, 6});
    g[2].push_back({1, 5});
    g[3].push_back({1, 4});
    g[3].push_back({2, 5});
    REQUIRE(Prima(g) == 12);
}