#include <iostream>
#include <queue>
#include <vector>

struct CVertex{
	int vertex = 0;
	int distance = 0;
};

void bfs( int start, const std::vector< std::vector<int> >& graph, std::vector<int>& dist )
{
	std::vector<bool> openedVertices( graph.size(), false );
	std::queue<CVertex> que;
	que.push( { start, 0 } );
	openedVertices[start] = true;
	while( !que.empty() ) {
		for( int i = 0; i < graph[que.front().vertex].size(); ++i ) {
			if( !openedVertices[graph[que.front().vertex][i]] ) { 
				// проверка того, что вершина не была посещена
				que.push( { graph[que.front().vertex][i], que.front().distance + 1 } );
				openedVertices[que.back().vertex] = true;
				dist[que.back().vertex] = que.back().distance;
			}
		}
		que.pop();
	}
}

int main()
{
	int numVertices = 0;
	int numEdges = 0;
	int leonVertex = 0;
	int matildaVertex = 0;
	int milkVertex = 0;

	std::cin >> numVertices >> numEdges >> leonVertex >> matildaVertex >> milkVertex;

	--leonVertex;
	--matildaVertex;
	--milkVertex;
	std::vector< std::vector<int> > graph( numVertices );
	for( int i = 0; i < numEdges; ++i ) {
		int x = 0;
		int y = 0;
		std::cin >> x >> y;
		--x;
		--y;
		graph[x].push_back( y );
		graph[y].push_back( x );
	}

	std::vector<int> leonDist( numVertices, 0 );
	bfs( leonVertex, graph, leonDist );

	std::vector<int> matildaDist( numVertices, 0 );
	bfs( matildaVertex, graph, matildaDist );

	std::vector<int> milkDist( numVertices, 0 );
	bfs( milkVertex, graph, milkDist );
	
	int shortestTotalRoute = numVertices;
	for( int i = 0; i < numVertices; ++i ) {
		shortestTotalRoute = std::min( leonDist[i] + matildaDist[i] + milkDist[i], shortestTotalRoute );
	}
	std::cout << shortestTotalRoute;
	return 0;
}