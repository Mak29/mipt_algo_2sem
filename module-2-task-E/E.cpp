#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "E.h"

TEST_CASE("Test 0")
{
    std::vector<CEdge> edges;
    int numVertices = 1;
    int numEdges = 0;
    int start = 0;
    double value = 0.0;
    REQUIRE(!CanTrade(edges, numVertices, start, value));
}

TEST_CASE("Test 1")
{
    std::vector<CEdge> edges;
    int numVertices = 3;
    int numEdges = 2;
    int start = 1 - 1;
    double value = 10.0;
    edges.push_back(CEdge{1 - 1, 2 - 1, 1.0, 1.0});
    edges.push_back(CEdge{1 - 1, 2 - 1, 1.0, 1.0});
    edges.push_back(CEdge{2 - 1, 3 - 1, 1.0, 1.1});
    edges.push_back(CEdge{2 - 1, 3 - 1, 1.0, 1.1});
    REQUIRE(!CanTrade(edges, numVertices, start, value));
}

TEST_CASE("Test 2")
{
    std::vector<CEdge> edges;
    int numVertices = 3;
    int numEdges = 2;
    int start = 1 - 1;
    double value = 10.0;
    edges.push_back(CEdge{1 - 1, 2 - 1, 1.0, 1.5});
    edges.push_back(CEdge{2 - 1, 1 - 1, 1.0, 1.0});
    edges.push_back(CEdge{2 - 1, 3 - 1, 1.0, 1.5});
    edges.push_back(CEdge{3 - 1, 2 - 1, 1.0, 1.1});
    REQUIRE(CanTrade(edges, numVertices, start, value));
}
