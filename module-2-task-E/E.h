
#include <iostream>
#include <vector>

struct CEdge
{
    int from = 0;
    int to = 0;
    double comission = 0.;
    double rate = 0.;
};

bool CanTrade(std::vector<CEdge> &edges, int numVerticies, int start, double value)
{
    std::vector<double> maxValue(numVerticies, 0.);
    maxValue[start] = value;
    for (int i = 0; i < numVerticies - 1; ++i)
    {
        for (auto edge : edges)
        {
            maxValue[edge.to] = std::max(maxValue[edge.to],
                                         (maxValue[edge.from] - edge.comission) * edge.rate);
        }
    }
    for (auto edge : edges)
    {
        if (maxValue[edge.to] < (maxValue[edge.from] - edge.comission) * edge.rate)
        {
            return true;
        }
    }
    return false;
}
/*
int main()
{
    int numVerticies = 0;
    int numEdges = 0;
    int start = 0;
    double value = 0;
    std::vector<CEdge> edges;
    std::cin >> numVerticies >> numEdges >> start >> value;
    for (int i = 0; i < numEdges; ++i)
    {
        CEdge ab;
        CEdge ba;
        std::cin >> ab.from >> ab.to;
        ba.to = --ab.from;
        ba.from = --ab.to;
        std::cin >> ab.rate >> ab.comission;
        std::cin >> ba.rate >> ba.comission;
        edges.push_back(ab);
        edges.push_back(ba);
    }
    std::cout << (canTrade(edges, numVerticies, --start, value) ? "YES" : "NO") << std::endl;
    return 0;
}
*/