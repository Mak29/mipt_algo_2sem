#include <bitset>
#include <climits>
#include <iostream>
#include <string>
#include <vector>


using ull = unsigned long long;

const int BITSET_SIZE = 1000;

void closure(std::vector<std::bitset<BITSET_SIZE>>& g)
{
    for (int k = 0; k < g.size(); ++k) {
        for (int i = 0; i < g.size(); ++i) {
            if (g[i][g.size() - k - 1]) {
                g[i] |= g[k];
            }
        }
    }
}

int main()
{
    int n = 0;
    std::cin >> n;
    std::vector<std::bitset<BITSET_SIZE>> g(n);
    for (int i = 0; i < n; ++i) {
        std::string s = "";
        std::cin >> s;
        g[i] = std::bitset<BITSET_SIZE>(s);
    }
    closure(g);
    for (int i = 0; i < n; ++i) {
        std::cout << g[i].to_string().substr(BITSET_SIZE - n, n) << std::endl;
    }
    return 0;
}
