#define CATCH_CONFIG_MATH
#include <catch2/catch.hpp>
#include "A.h"

TEST_CASE("zeroPath") {
    REQUIRE(FindWay(15, 3, 14, 9, 9) == 0);
}

TEST_CASE("easyPath") {
    REQUIRE(FindWay(6, 1, 5, 2, 3) == 6);
}

TEST_CASE("differentPath") {
    REQUIRE(FindWay(6, 1, 5, 2, 1) == 2);
}