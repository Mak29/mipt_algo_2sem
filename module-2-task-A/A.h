#include <cassert>
#include <climits>
#include <cmath>
#include <iostream>
#include <queue>
#include <vector>

using ull = unsigned long long;

struct CVertex
{
    ull number = 0;
    ull dist = 0;

    CVertex(ull number, ull dist) : number(number), dist(dist)
    {
    }
};

bool operator<(const CVertex &u, const CVertex &v)
{
    if (u.dist == v.dist) {
        return (u.number > v.number);
    }
    return (u.dist > v.dist);
}

void TryUpdate(ull who, std::priority_queue<CVertex>& where, std::vector<ull>& dist, ull newDist)
{
    if (newDist >= dist[who]) {
        return;
    }
    dist[who] = newDist;
    where.push({who, newDist});
}

ull FindWay(ull m, ull a, ull b, ull from, ull to)
{
    std::vector<ull> dist(m, ULONG_LONG_MAX);
    std::vector<bool> is(m, false);
    std::priority_queue<CVertex> undiscovered;
    undiscovered.push({from, 0});
    dist[from] = 0;
    for (int count = 0; count < m; ++count) {
        CVertex newVertex = undiscovered.top();
        undiscovered.pop();
        while (is[newVertex.number]) {
            newVertex = undiscovered.top();
            undiscovered.pop();
        }
        is[newVertex.number] = true;
        if (newVertex.number == to) {
            return newVertex.dist;
        }
        TryUpdate((newVertex.number + 1) % m, undiscovered, dist, newVertex.dist + a);
        TryUpdate((newVertex.number * newVertex.number + 1) % m, undiscovered, dist,
            newVertex.dist + b);
    }
    return dist[to];
}
