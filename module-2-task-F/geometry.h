#include <cassert>
#include <cmath>
#include <vector>

const double DELTA = 1e-14;

bool IsLess(double one, double two)
{
    return one + DELTA < two;
}

int Sign(double x)
{
    if (IsLess(0., x)) {
        return 1;
    }
    if (IsLess(x, 0.)) {
        return -1;
    }
    return 0;
}

double Sqr(const double& x)
{
    return x * x;
}

bool IsEqual(double one, double two)
{
    return std::abs(one - two) < DELTA;
}

bool IsLessOrEqual(double one, double two)
{
    return one < two + DELTA;
}

class CVector;
class CLine;
class CPolygon;
class CEllipse;

struct CPoint {
    double x = 0.;
    double y = 0.;

    CPoint() = default;
    CPoint(double x, double y);

    bool operator==(const CPoint& other) const;
    bool operator!=(const CPoint& other) const;

    CPoint& operator+=(const CVector& a);

    CPoint& Rotate(const CPoint& Center, double angle);
    CPoint& Scale(const CPoint& Center, double k);
    CPoint& Reflex(const CPoint& Center);
    CPoint& Reflex(const CLine& l);
};
/*
#include <iostream>

std::ostream& operator<<(std::ostream& out, CPoint p)
{
    out << "(" << p.x << ", " << p.y << ")";
    return out;
}
*/
struct CVector {
public:
    double x = 0.;
    double y = 0.;
    CVector() = default;
    CVector(double, double);
    CVector(const CPoint& a, const CPoint& b);

    CVector& Rotate(double angle);
    CVector& Scale(double k);
    double Length();
};

double operator*(const CVector& one, const CVector& two);

class CLine {
public:
    CLine(CPoint p, CVector l);
    CLine(CPoint one, CPoint two);
    CLine(CPoint p, double k);
    CLine(double k, double b);

    bool operator!=(const CLine& other);
    bool operator==(const CLine& other);

    int Semiplane(const CPoint& p) const;
    CVector Normal() const;

    CPoint p;
    CVector l;
};

class IShape {
public:
    virtual double Perimeter() const = 0;
    virtual double Area() const = 0;
    virtual bool operator==(const IShape& another) const = 0;
    virtual bool IsCongruentTo(const IShape& another) const = 0;
    virtual bool IsSimilarTo(const IShape& another) const = 0;
    virtual bool ContainsPoint(const CPoint&) const = 0;
    virtual void Rotate(CPoint Center, double angle) = 0;
    virtual void Reflex(CPoint Center) = 0;
    virtual void Reflex(CLine axis) = 0;
    virtual void Scale(CPoint Center, double coefficient) = 0;

    virtual ~IShape() {};
};

class CPolygon : public IShape {
public:
    CPolygon(const std::vector<CPoint>& points);
    template <typename... Tail>
    CPolygon(const Tail&... args);

    std::size_t VerticiesCount() const;
    const std::vector<CPoint>& GetVertices() const;
    bool IsConvex() const;

    double Perimeter() const override;
    double Area() const override;
    bool operator==(const IShape& another) const override;
    bool IsCongruentTo(const IShape& another) const override;
    bool IsSimilarTo(const IShape& another) const override;
    bool ContainsPoint(const CPoint&) const override;
    void Rotate(CPoint Center, double angle) override;
    void Reflex(CPoint Center) override;
    void Reflex(CLine axis) override;
    void Scale(CPoint Center, double coefficient) override;

    CVector Edge(int i) const;
    const CPoint& V(int i) const;

protected:
    std::vector<CPoint> vertex;

private:
    bool isDividedByLine(const CLine& l) const;

    void toVector() {}
    template <typename... Tail>
    void toVector(const CPoint& first, const Tail&... args);

    bool isOnEdge(const CPoint& p, int i) const;
};

class CEllipse : public IShape {
public:
    CEllipse(const CPoint& f1, const CPoint& f2, double r);

    std::pair<CPoint, CPoint> Focuses() const;
    virtual std::pair<CLine, CLine> Directrices() const;
    virtual double Eccentricity() const;
    CPoint Center() const;

    double Perimeter() const override;
    double Area() const override;
    bool operator==(const IShape& another) const override;
    bool IsCongruentTo(const IShape& another) const override;
    bool IsSimilarTo(const IShape& another) const override;
    bool ContainsPoint(const CPoint&) const override;
    void Rotate(CPoint Center, double angle) override;
    void Reflex(CPoint Center) override;
    void Reflex(CLine axis) override;
    void Scale(CPoint Center, double coefficient) override;

protected:
    CPoint f1;
    CPoint f2;
    double a = 0.;

    CEllipse() {}

    double countb() const;
};

class CCircle : public CEllipse {
public:
    CCircle(const CPoint& p, double r);

    double Radius() const;

    double Perimeter() const override;
    double Area() const override;
};

class CRectangle : public CPolygon {
public:
    CRectangle() = default;
    CRectangle(const CPoint& one, const CPoint& two, double k);

    CPoint Center() const;
    std::pair<CLine, CLine> Diagonals() const;
};

class CSquare : public CRectangle {
public:
    CSquare(const CPoint& one, const CPoint& two)
        : CRectangle(one, two, 1.)
    {
    }

    CCircle CircumscribedCircle() const;
    CCircle InscribedCircle() const;
};

class CTriangle : public CPolygon {
public:
    CTriangle(const CPoint& one, const CPoint& two, const CPoint& three);

    CCircle CircumscribedCircle() const;
    CCircle InscribedCircle() const;
    CPoint Centroid() const;
    CPoint Orthocenter() const;
    CLine EulerLine() const;
    CCircle NinePointsCircle() const;
    double Area() const override;
    double directedArea() const;

    CLine Bis(int i) const;
    CLine Med(int i) const;
    CLine Height(int i) const;
};

bool operator!=(const IShape& first, const IShape& second)
{
    return !(first == second);
}

CPoint& CPoint::operator+=(const CVector& a)
{
    x += a.x;
    y += a.y;
    return *this;
}

CPoint operator+(const CPoint& one, const CVector& two)
{
    CPoint p = one;
    return p += two;
}

CPoint Intersection(const CLine& one, const CLine& two)
{
    double d = one.l.x * two.l.y - two.l.x * one.l.y;
    assert(d != 0);
    CPoint p;
    p.x = one.l.x * two.l.x * (one.p.y - two.p.y);
    p.x += one.l.x * two.l.y * two.p.x - two.l.x * one.l.y * one.p.x;
    p.x /= d;
    p.y = one.l.y * two.l.y * (two.p.x - one.p.x);
    p.y += one.l.x * two.l.y * one.p.y - two.l.x * one.l.y * two.p.y;
    p.y /= d;
    return p;
}

CLine serper(const CPoint&, const CPoint&);

// Точка

CPoint::CPoint(double x, double y)
{
    this->x = x;
    this->y = y;
}

bool CPoint::operator==(const CPoint& other) const
{
    return IsEqual(x, other.x) && (IsEqual(y, other.y));
}
bool CPoint::operator!=(const CPoint& other) const
{
    return !((*this) == other);
}
CPoint& CPoint::Rotate(const CPoint& Center, double angle)
{
    (*this) = Center + CVector(Center, *this).Rotate(angle);
    return *this;
}
CPoint& CPoint::Scale(const CPoint& Center, double k)
{
    (*this) = Center + CVector(Center, *this).Scale(k);
    return *this;
}
CPoint& CPoint::Reflex(const CPoint& Center)
{
    (*this) = Center + CVector(Center, *this).Scale(-1);
    return *this;
}
CPoint& CPoint::Reflex(const CLine& l)
{
    CVector n = l.Normal();
    CVector a(l.p, *this);
    (*this) += n.Scale((a * n) / (n * n) * (-2.));
    return *this;
}

CPoint Middle(const CPoint& one, const CPoint& two)
{
    return one + CVector(one, two).Scale(0.5);
}

// Вектор

CVector::CVector(double x, double y) : x(x), y(y)
{
}
CVector::CVector(const CPoint& a, const CPoint& b)
{
    x = b.x - a.x;
    y = b.y - a.y;
}

CVector operator+(const CVector& one, const CVector& two)
{
    return CVector(one.x + two.x, one.y + two.y);
}

double operator*(const CVector& one, const CVector& two)
{
    return one.x * two.x + one.y * two.y;
}

CVector& CVector::Rotate(double angle)
{
    double oldx = x;
    double oldy = y;
    x = oldx * cos(angle) - oldy * sin(angle);
    y = oldx * sin(angle) + oldy * cos(angle);
    return *this;
}
CVector& CVector::Scale(double k)
{
    x *= k;
    y *= k;
    return *this;
}
double CVector::Length()
{
    return sqrt((*this) * (*this));
}
double Dist(const CPoint& a, const CPoint& b)
{
    return CVector(a, b).Length();
}
bool IsCollinear(const CVector& one, const CVector& two)
{
    return IsEqual(one.x * two.y, two.x * one.y);
}

// Прямая

CLine::CLine(CPoint p, CVector l)
    : p(p)
    , l(l)
{
}
CLine::CLine(CPoint one, CPoint two)
{
    p = one;
    l = CVector(one, two);
}
CLine::CLine(CPoint p, double k)
    : p(p)
{
    l.x = 1.;
    l.y = k;
}
CLine::CLine(double k, double b)
{
    (*this) = CLine(CPoint(0, b), k);
}
bool CLine::operator!=(const CLine& other)
{
    return !((*this) == other);
}
bool CLine::operator==(const CLine& other)
{
    return IsCollinear(l, other.l) && (!Semiplane(other.p));
}
int CLine::Semiplane(const CPoint& p) const
{
    CVector n = Normal();
    double value = n * CVector(this->p, p);
    return (IsLess(value, 0.) ? -1 : (IsEqual(value, 0.) ? 0 : 1));
}
CVector CLine::Normal() const
{
    CVector n = l;
    return n.Rotate(M_PI / 2.);
}

// Многоугольник

CPolygon::CPolygon(const std::vector<CPoint>& points)
{
    vertex = points;
}

template <typename... Tail>
CPolygon::CPolygon(const Tail&... args)
{
    toVector(args...);
}
template <typename... Tail>
void CPolygon::toVector(const CPoint& first, const Tail&... args)
{
    vertex.push_back(first);
    toVector(args...);
}
bool CPolygon::IsConvex() const
{
    if (isDividedByLine(CLine(vertex[0], vertex.back()))) {
        return false;
    }
    for (std::size_t i = 0; i < vertex.size() - 1; ++i) {
        if (isDividedByLine(CLine(vertex[i], vertex[i + 1]))) {
            return false;
        }
    }
    return true;
}
double CPolygon::Perimeter() const
{
    double p = Dist(vertex[0], vertex.back());
    for (std::size_t i = 0; i < vertex.size() - 1; ++i) {
        p += Dist(vertex[i], vertex[i + 1]);
    }
    return p;
}
double CPolygon::Area() const
{
    double s = 0.;
    for (std::size_t i = 1; i < vertex.size() - 1; ++i) {
        s += CTriangle(vertex[0], vertex[i], vertex[i + 1]).directedArea();
    }
    return std::abs(s);
}
bool CPolygon::operator==(const IShape& another) const
{
    if (!dynamic_cast<const CPolygon*>(&another)) {
        return false;
    }
    const CPolygon& other = dynamic_cast<const CPolygon&>(another);
    if (vertex.size() != other.vertex.size()) {
        return false;
    }
    for (std::size_t i = 0; i < vertex.size(); ++i) {
        bool f = true;
        for (std::size_t j = 0; j < vertex.size(); ++j) {
            if (V(j) != other.V(i + j)) {
                f = false;
                break;
            }
        }
        if (f) {
            return true;
        }
        f = true;
        for (std::size_t j = 0; j < vertex.size(); ++j) {
            if (V(-static_cast<int>(j) - 1) != other.V(i + j)) {
                f = false;
                break;
            }
        }
        if (f) {
            return true;
        }
    }
    return false;
}
bool CPolygon::IsCongruentTo(const IShape& another) const
{
    if (!dynamic_cast<const CPolygon*>(&another)) {
        return false;
    }
    const CPolygon& other = dynamic_cast<const CPolygon&>(another);
    if (vertex.size() != other.vertex.size()) {
        return false;
    }
    for (std::size_t i = 0; i < vertex.size(); ++i) {
        bool f = true;
        for (std::size_t j = 0; j < vertex.size(); ++j) {
            if (Edge(j).Length() == other.Edge(i + j).Length()
                && Edge(j) * Edge(j + 1) == other.Edge(i + j) * other.Edge(i + j + 1)) {
                f = false;
                break;
            }
        }
        if (f) {
            return true;
        }
        f = true;
        for (int j = 0; j < static_cast<int>(vertex.size()); ++j) {
            if (Edge(-j).Length() == other.Edge(i + j).Length() 
                && Edge(-j) * Edge(-j - 1) == other.Edge(i + j) * other.Edge(i + j + 1)) {
                f = false;
                break;
            }
        }
        if (f) {
            return true;
        }
    }
    return false;
}
bool CPolygon::IsSimilarTo(const IShape& another) const
{
    if (!dynamic_cast<const CPolygon*>(&another)) {
        return false;
    }
    const CPolygon& other = dynamic_cast<const CPolygon&>(another);
    if (VerticiesCount() != other.VerticiesCount()) {
        return false;
    }
    for (std::size_t i = 0; i < vertex.size(); ++i) {
        CPolygon p = other;
        p.Scale(CPoint(0, 0), Edge(i).Length() / other.Edge(0).Length());
        if (IsCongruentTo(p)) {
            return true;
        }
    }
    return false;
}
bool CPolygon::ContainsPoint(const CPoint& p) const
{
    int k = 0;
    CLine l(p, CVector(1, 0));
    for (std::size_t i = 0; i < vertex.size(); ++i) {
        if (isOnEdge(p, i)) {
            return true;
        }
        CLine e(vertex[i], Edge(i));
        if (!IsCollinear(l.l, e.l)) {
            CPoint c = Intersection(l, e);
            k += (isOnEdge(c, i) && c.x >= p.x);
            if (c == V(i + 1) && c.x >= p.x) {
                CLine l1(V(i + 1), Edge(i));
                CLine l2(V(i + 1), Edge(i + 1));
                if (l1.Semiplane(p) * l2.Semiplane(p) == 1) {
                    --k;
                }
            }
        } else if (!e.Semiplane(p) && V(i).x >= p.x) {
            CLine l1(V(i), Edge(i - 1));
            CLine l2(V(i + 1), Edge(i + 1));
            if (l1.Semiplane(p) * l2.Semiplane(p) == 1) {
                --k;
            }
        }
    }
    return (k % 2);
}
void CPolygon::Rotate(CPoint Center, double angle)
{
    for (std::size_t i = 0; i < vertex.size(); ++i) {
        vertex[i].Rotate(Center, angle);
    }
}
void CPolygon::Reflex(CPoint Center)
{
    for (std::size_t i = 0; i < vertex.size(); ++i) {
        vertex[i].Reflex(Center);
    }
}
void CPolygon::Reflex(CLine axis)
{
    for (std::size_t i = 0; i < vertex.size(); ++i) {
        vertex[i].Reflex(axis);
    }
}
void CPolygon::Scale(CPoint Center, double coefficient)
{
    for (std::size_t i = 0; i < vertex.size(); ++i) {
        vertex[i].Scale(Center, coefficient);
    }
}
std::size_t CPolygon::VerticiesCount() const
{
    return vertex.size();
}
const std::vector<CPoint>& CPolygon::GetVertices() const
{
    return vertex;
}
bool CPolygon::isDividedByLine(const CLine& l) const
{
    bool isPositive = false;
    bool isNegative = false;
    for (auto x : vertex) {
        int value = l.Semiplane(x);
        if (value > 0) {
            isPositive = true;
        } else if (value < 0) {
            isNegative = true;
        }
    }
    return (isPositive && isNegative);
}

CVector CPolygon::Edge(int i) const
{
    if (V(i) == vertex.back()) {
        return CVector(vertex.back(), vertex[0]);
    }
    return CVector(V(i), V(i + 1));
}
const CPoint& CPolygon::V(int i) const
{
    return vertex[(i % vertex.size() + vertex.size()) % vertex.size()];
}
bool CPolygon::isOnEdge(const CPoint& p, int i) const
{
    if (CLine(vertex[i], Edge(i)).Semiplane(p)) {
        return false;
    }
    if (vertex[i] == p || V(i + 1) == p) {
        return true;
    }
    if (IsEqual(vertex[i].x, V(i + 1).x)) {
        return (IsLess(vertex[i].y, p.y) ^ IsLess(V(i + 1).y, p.y));
    } else {
        return (IsLess(vertex[i].x, p.x) ^ IsLess(V(i + 1).x, p.x));
    }
}
// ЭллипC

CEllipse::CEllipse(const CPoint& f1, const CPoint& f2, double r)
    : f1(f1)
    , f2(f2)
    , a(r / 2.)
{
}
std::pair<CPoint, CPoint> CEllipse::Focuses() const
{
    return { f1, f2 };
}
std::pair<CLine, CLine> CEllipse::Directrices() const
{
    CVector l = CLine(f1, f2).Normal();
    CPoint one = Center() + CVector(f1, f2).Scale(0.5 / Sqr(Eccentricity()));
    CPoint two = Center() + CVector(f1, f2).Scale(-0.5 / Sqr(Eccentricity()));
    return { CLine(one, l), CLine(two, l) };
}
double CEllipse::Eccentricity() const
{
    return Dist(f1, f2) / (2. * a);
}
CPoint CEllipse::Center() const
{
    return Middle(f1, f2);
}

double Delta(int n, double a, double b)
{
    static double d;
    if (n == 1) {
        d = (a - b) / (a + b) / 2.;
        return d;
    }
    d *= (2. * n - 3) / (2. * n) * (a - b) / (a + b);
    return d;
}
double CEllipse::Perimeter() const
{
    double b = countb();
    int n = 1;
    double old = 0;
    double d = Sqr(Delta(n++, a, b));
    double s = d;
    while (!IsEqual(old, d)) {
        old = d;
        d = Sqr(Delta(n++, a, b));
        s += d;
    }
    return M_PI * (a + b) * (1. + s);
}
double CEllipse::Area() const
{
    return M_PI * a * countb();
}
bool CEllipse::operator==(const IShape& another) const
{
    if (!dynamic_cast<const IShape*>(&another)) {
        return false;
    }
    const CEllipse& other = dynamic_cast<const CEllipse&>(another);
    return (f1 == other.f1) && (f2 == other.f2) && (a == other.a);
}
bool CEllipse::IsCongruentTo(const IShape& another) const
{
    if (!dynamic_cast<const IShape*>(&another)) {
        return false;
    }
    const CEllipse& other = dynamic_cast<const CEllipse&>(another);
    return (a == other.a) && (countb() == other.countb());
}
bool CEllipse::IsSimilarTo(const IShape& another) const
{
    if (!dynamic_cast<const IShape*>(&another)) {
        return false;
    }
    const CEllipse& other = dynamic_cast<const CEllipse&>(another);
    return IsEqual(a * other.countb(), countb() * other.a);
}
bool CEllipse::ContainsPoint(const CPoint& p) const
{
    return IsLessOrEqual(
        Sqr((p.x - Center().x) / a) + Sqr((p.y - Center().y) / countb()), 1.);
}
void CEllipse::Rotate(CPoint Center, double angle)
{
    f1.Rotate(Center, angle);
    f2.Rotate(Center, angle);
}
void CEllipse::Reflex(CPoint Center)
{
    f1.Reflex(Center);
    f2.Reflex(Center);
}
void CEllipse::Reflex(CLine axis)
{
    f1.Reflex(axis);
    f2.Reflex(axis);
}
void CEllipse::Scale(CPoint Center, double coefficient)
{
    f1.Scale(Center, coefficient);
    f2.Scale(Center, coefficient);
    a *= coefficient;
}
double CEllipse::countb() const
{
    return sqrt(Sqr(a) - Sqr(Dist(f1, f2) / 2.));
}

// ОкружноCть

CCircle::CCircle(const CPoint& p, double r)
{
    f1 = p;
    f2 = p;
    a = r;
}
double CCircle::Radius() const
{
    return a;
}
double CCircle::Perimeter() const
{
    return 2 * M_PI * a;
}
double CCircle::Area() const
{
    return M_PI * Sqr(a);
}

// Прямоугольник

CRectangle::CRectangle(const CPoint& one, const CPoint& three, double k)
{
    assert(IsLess(0., k));
    if (k < 1.) {
        k = 1. / k;
    }
    double a = atan(k);
    CVector d(three, one);
    CLine s1(one, d.Rotate(a));
    CLine s2(three, d.Rotate(-M_PI / 2.));
    CPoint two = Intersection(s1, s2);
    CPoint four = three + CVector(two, one);
    vertex.push_back(one);
    vertex.push_back(two);
    vertex.push_back(three);
    vertex.push_back(four);
}
CPoint CRectangle::Center() const
{
    return Middle(vertex[0], vertex[2]);
}
std::pair<CLine, CLine> CRectangle::Diagonals() const
{
    return { CLine(vertex[0], vertex[3]), CLine(vertex[2], vertex[4]) };
}

// Квадрат

CCircle CSquare::CircumscribedCircle() const
{
    return CCircle(Center(), Dist(Center(), vertex[0]));
}
CCircle CSquare::InscribedCircle() const
{
    return CCircle(Center(), Dist(vertex[0], vertex[1]) / 2.);
}

// Треугольник

CTriangle::CTriangle(const CPoint& one, const CPoint& two, const CPoint& three)
{
    vertex.push_back(one);
    vertex.push_back(two);
    vertex.push_back(three);
}
CCircle CTriangle::CircumscribedCircle() const
{
    CPoint Center = Intersection(serper(vertex[0], vertex[1]), serper(vertex[0], vertex[2]));
    return CCircle(Center, Dist(Center, vertex[0]));
}
CCircle CTriangle::InscribedCircle() const
{
    CPoint p = Intersection(Bis(0), Bis(1));
    return CCircle(p, 2 * Area() / Perimeter());
}
CPoint CTriangle::Centroid() const
{
    return Intersection(Med(0), Med(1));
}
CPoint CTriangle::Orthocenter() const
{
    return Intersection(Height(0), Height(1));
}
CLine CTriangle::EulerLine() const
{
    return CLine(Centroid(), Orthocenter());
}
CCircle CTriangle::NinePointsCircle() const
{
    CCircle c = CircumscribedCircle();
    c.Scale(Orthocenter(), 0.5);
    return c;
}

double CTriangle::directedArea() const
{
    double s1 = (vertex[0].x - vertex[2].x) * (vertex[1].y - vertex[2].y);
    double s2 = (vertex[0].y - vertex[2].y) * (vertex[1].x - vertex[2].x);
    return (s1 - s2) / 2.;
}
double CTriangle::Area() const
{
    return std::abs(directedArea());
}

CLine CTriangle::Bis(int i) const
{
    return CLine(V(i),
        CVector(V(i), V(i + 1)) + CVector(V(i + 1), V(i + 2)).Scale(Dist(V(i), V(i + 1)) / (Dist(V(i), V(i + 1)) + Dist(V(i), V(i + 2)))));
}
CLine CTriangle::Med(int i) const
{
    return CLine(V(i), Middle(V(i + 1), V(i + 2)));
}
CLine CTriangle::Height(int i) const
{
    return CLine(V(i), CLine(V(i + 1), V(i + 2)).Normal());
}
CLine serper(const CPoint& a, const CPoint& b)
{
    return CLine(Middle(a, b), CLine(a, b).Normal());
}
