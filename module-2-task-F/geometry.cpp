#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "geometry.h"

TEST_CASE("Test area")
{
    REQUIRE(IsEqual(CSquare(CPoint(1, -1), CPoint(-0.5, 2)).area(), 5.625));
}

TEST_CASE("Test triangle circles")
{
    CTriangle abc(CPoint(-2,-1), CPoint(3, 11), CPoint(12, -1));
    CCircle w = abc.circumscribedCircle();
    CCircle ins = abc.inscribedCircle();
    double R = w.radius();
    double r = ins.radius();
    
    REQUIRE(IsEqual(Dist(w.center(), ins.center()), sqrt(R * R - 2 * r * R)));
}

TEST_CASE("Test motions, ==, Euler circle and line")
{
    CTriangle abc(CPoint(-2,-1), CPoint(3, 11), CPoint(12, -1));
    abc.reflex(CPoint(1, 3));
    CCircle w = abc.circumscribedCircle();
    w.scale(CPoint(abc.orthocenter()), 0.5);
    REQUIRE(w == abc.ninePointsCircle());
    REQUIRE((abc.EulerLine() == CLine(abc.centroid(), w.center())));
}
