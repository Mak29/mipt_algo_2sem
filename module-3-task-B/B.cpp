#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "B.h"

TEST_CASE("Test 0")
{
    int n = 1;
    int m = 0;
    std::vector<CEdge> g;
    REQUIRE(Kruskal(n, g) == 0);
}

TEST_CASE("Test 1")
{
    int n = 2;
    int m = 1;
    std::vector<CEdge> g;
    g.push_back({1 - 1, 2 - 1, 10986});
    g.push_back({2 - 1, 1 - 1, 10986});
    REQUIRE(Kruskal(n, g) == 10986);
}

TEST_CASE("Test 2")
{
    int n = 4;
    int m = 6;
    std::vector<CEdge> g;
    g.push_back({0, 1, 6});
    g.push_back({0, 2, 3});
    g.push_back({0, 3, 6});
    g.push_back({1, 2, 5});
    g.push_back({1, 3, 4});
    g.push_back({2, 3, 5});
    g.push_back({1, 0, 6});
    g.push_back({2, 0, 3});
    g.push_back({3, 0, 6});
    g.push_back({2, 1, 5});
    g.push_back({3, 1, 4});
    g.push_back({3, 2, 5});
    REQUIRE(Kruskal(n, g) == 12);
}
