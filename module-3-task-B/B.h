#include <algorithm>
#include <iostream>
#include <vector>

struct CEdge{
    int from;
    int to;
    int weight;
};

bool operator< (const CEdge& u, const CEdge& v)
{
    return u.weight < v.weight;
}

class CDsu{
public:
    CDsu(int n) : parent(n), size(n, 1) {
        for (int i = 0; i < n; ++i) {
            parent[i] = i;
        }
    }

    int get(int i)
    {
        if (i == parent[i]) {
            return i;
        }
        return parent[i] = get(parent[i]);
    }

    void unite(int i, int j)
    {
        i = get(i);
        j = get(j);
        if (size[i] < size[j]) {
            std::swap(i, j);
        }
        parent[j] = i;
        size[i] += size[j];
    }
private:
    std::vector<int> parent;
    std::vector<int> size;
};

long long Kruskal(size_t numVertices, std::vector<CEdge>& g)
{
    std::sort(g.begin(), g.end());
    CDsu dsu(numVertices);
    long long sum = 0;
    for (auto edge : g) {
        if (dsu.get(edge.from) != dsu.get(edge.to)) {
            dsu.unite(edge.from, edge.to);
            sum += edge.weight;
        }
    }
    return sum;
}

/*
int main()
{
    int numVertices = 0;
    int numEdges = 0;
    std::cin >> numVertices >> numEdges;
    std::vector<CEdge> g(numVertices);
    for (int i = 0; i < numEdges; ++i) {
        int from = 0;
        int to = 0;
        int weight = 0;
        std::cin >> from >> to >> weight;
        g.push_back({--from, --to, weight});
        g.push_back({to, from, weight});
    }
    std::cout << Kruskal(numVertices, g) << std::endl;
    return 0;
}
*/