#include <iostream>
#include <set>
#include <string>
#include <vector>

bool Find(const std::vector<std::vector<int>>& g, std::vector<bool>& used, int s, int t, std::vector<int>& path)
{
    path.push_back(s);
    used[s] = true;
    if (s == t) {
        return true;
    }
    for (int i = 0; i < g[s].size(); ++i) {
        if (g[s][i] && !used[i] && Find(g, used, i, t, path)) {
            return true;
        }
    }
    path.pop_back();
    return false;
}

std::ostream& operator<< (std::ostream& out, const std::vector<int>& v)
{
    for (auto x : v) {
        std::cout << x << ' ';
    }
    out << std::endl;
    return out;
}

bool Path(std::vector<std::vector<int>>& g, int s, int t)
{
    std::vector<bool> used(g.size(), false);
    std::vector<int> path;
    if (!Find(g, used, s, t, path)) {
        return false;
    }
    for (auto it = path.begin(); it != path.end() - 1; ++it) {
        --g[*it][*(it + 1)];
        ++g[*(it + 1)][*it];
    }
    return true;
}

int Flow(std::vector<std::vector<int>>& g, int t)
{
    int k = 0;
    while (Path(g, 0, t)) {
        ++k;
    }
    return k;
}

void Dfs(const std::vector<std::vector<int>>& g, int v, int to, std::vector<bool>& used) {
    used[v] = true;
    if (v == to) {
        return;
    }
    for (int i = 0; i < g[v].size(); ++i) {
        if (g[v][i] && !used[i]) {
            Dfs(g, i, to, used);
        }
    }
}

void Solve(std::vector<std::vector<int>>& g, std::vector<int>& half1, std::vector<int>& half2)
{
    int num = 0;
    int minFlow = g.size() + 1;
    for (int i = 1; i < g.size(); ++i) {
        std::vector<std::vector<int>> cg = g;
        int flow = Flow(cg, i);
        if (flow < minFlow) {
            num = i;
            minFlow = flow;
        }
    }
    half1.clear();
    half2.clear();
    Flow(g, num);
    std::vector<bool> used(g.size(), false);
    Dfs(g, 0, num, used);
    for (int i = 0; i < used.size(); ++i) {
        (used[i] ? half1 : half2).push_back(i + 1);
    }
}

/*
int main()
{
    int n = 0;
    std::cin >> n;
    std::vector<std::vector<int>> g(n);
    for (int i = 0; i < n; ++i) {
        std::string s;
        std::cin >> s;
        for (int j = 0; j < s.size(); ++j) {
            g[i].push_back(s[j] - '0');
        }
    }
    std::vector<int> half1;
    std::vector<int> half2;
    Solve(g, half1, half2);
    std::cout << half1 << half2;
    return 0;
}
*/