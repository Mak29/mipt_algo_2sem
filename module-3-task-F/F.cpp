#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "F.h"

TEST_CASE("Test 0")
{
    std::vector<std::vector<int>> g;
    g.push_back({0, 0});
    g.push_back({0, 0});
    std::vector<int> half1;
    std::vector<int> half2;
    Solve(g, half1, half2);
    REQUIRE(half1 == std::vector<int>{1});
    REQUIRE(half2 == std::vector<int>{2});
}

TEST_CASE("Test 1")
{
    std::vector<std::vector<int>> g;
    g.push_back({0, 1, 1, 1});
    g.push_back({1, 0, 0, 1});
    g.push_back({1, 0, 0, 1});
    g.push_back({1, 1, 1, 0});
    std::vector<int> half1;
    std::vector<int> half2;
    Solve(g, half1, half2);
    REQUIRE(half1 == std::vector<int>{1, 3, 4});
    REQUIRE(half2 == std::vector<int>{2});
}

TEST_CASE("Test 2")
{
    std::vector<std::vector<int>> g;
    g.push_back({0, 1, 1, 0, 0, 0});
    g.push_back({1, 0, 1, 0, 0, 0});
    g.push_back({1, 1, 0, 1, 0, 0});
    g.push_back({0, 0, 1, 0, 1, 1});
    g.push_back({0, 0, 0, 1, 0, 1});
    g.push_back({0, 0, 0, 1, 1, 0});
    std::vector<int> half1;
    std::vector<int> half2;
    Solve(g, half1, half2);
    REQUIRE(half1 == std::vector<int>{1, 2, 3});
    REQUIRE(half2 == std::vector<int>{4, 5, 6});
}