#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "D.h"

TEST_CASE("Test 0")
{
    std::vector<std::vector<int>> g(9);
    REQUIRE(CountInPair(g) == 0);
}

TEST_CASE("Test 1")
{
    std::vector<std::vector<int>> g(6);
    g[1].push_back(2);
    g[2].push_back(1);
    g[1].push_back(4);
    g[4].push_back(1);
    REQUIRE(CountInPair(g) == 2);
}

TEST_CASE("Test 2")
{
    std::vector<std::vector<int>> g(6);
    g[0].push_back(1);
    g[1].push_back(0);
    g[0].push_back(3);
    g[3].push_back(0);
    g[1].push_back(2);
    g[2].push_back(1);
    REQUIRE(CountInPair(g) == 4);
}