#include <iostream>
#include <vector>
#include <string>

bool kuhn(std::vector<bool>& used, const std::vector<std::vector<int>>& g,
    std::vector<int>& pair, int v)
{
    if (used[v]) {
        return false;
    }
    used[v] = true;
    for (int to : g[v]) {
        if (pair[to] == -1 || kuhn(used, g, pair, pair[to])) {
            pair[to] = v;
            return true;
        } 
    }
    return false;
}

int CountInPair(const std::vector<std::vector<int>>& g)
{
    std::vector<bool> used(g.size(), false);
    std::vector<int> pair(g.size(), -1);
    for (int i = 0; i < g.size(); ++i) {
        if (kuhn(used, g, pair, i)) {
            used.assign(g.size(), false);
        }
    }
    int k = 0;
    for (int x : pair) {
        k += (x != -1);
    }
    return k;
}
/*
int main()
{
    int n = 0;
    int m = 0;
    int a = 0;
    int b = 0;
    std::cin >> n >> m >> a >> b;
    if (2 * b <= a) {
        int k = 0;
        for (int i = 0; i < n; ++i) {
            std::string s;
            std::cin >> s;
            for (int j = 0; j < m; ++j) {
                if (s[j] == '*') {
                    ++k;
                }
            }
        }
        std::cout << k * b << std::endl;
    } else {
        std::vector<std::vector<int>> g(n * m);
        std::vector<std::string> bridge(n);
        int k = 0;
        for (int i = 0; i < n; ++i) {
            std::cin >> bridge[i];
            for (int j = 0; j < m; ++j) {
                if (bridge[i][j] == '*') {
                    ++k;
                    if (j > 0 && bridge[i][j - 1] == '*') {
                        g[i * m + j].push_back(i * m + j - 1);
                        g[i * m + j - 1].push_back(i * m + j);
                    }
                    if (i > 0 && bridge[i - 1][j] == '*') {
                        g[(i - 1) * m + j].push_back(i * m + j);
                        g[i * m + j].push_back((i - 1) * m + j);
                    }
                }
            }
        }
        int count = CountInPair(g);
        std::cout << count / 2 * a + (k - count) * b << std::endl;
    }
    return 0;
}
*/