#include <iostream>
#include <vector>
#include <set>

bool Find(const std::vector<std::multiset<int>>& g, std::vector<bool>& used, int s, int t, std::vector<int>& path)
{
    path.push_back(s);
    used[s] = true;
    if (s == t) {
        return true;
    }
    for (auto v : g[s]) {
        if (!used[v] && Find(g, used, v, t, path)) {
            return true;
        }
    }
    path.pop_back();
    return false;
}

std::ostream& operator<< (std::ostream& out, std::vector<int> v)
{
    for (auto x : v) {
        std::cout << x << ' ';
    }
    out << std::endl;
    return out;
}

bool Path(std::vector<std::multiset<int>>& g, int s, int t, std::vector<int>& path, std::vector<int>& index)
{
    std::vector<bool> used(g.size(), false);
    if (!Find(g, used, s, t, path)) {
        return false;
    }
    index.assign(g.size(), -1);
    for (auto it = path.begin(); it != path.end() - 1; ++it) {
        g[*it].erase(g[*it].find(*(it + 1)));
        g[*(it + 1)].insert(*it);
        index[*it] = it - path.begin(); 
    }
    index[*(path.end() - 1)] = path.end() - path.begin() - 1;
    return true;
}

bool Flow(std::vector<std::multiset<int>> g, int s, int t, std::vector<int>& path0, std::vector<int>& path1)
{
    path0.clear();
    path1.clear();
    if (s == t) {
        path0.push_back(s);
        path1.push_back(s);
        return true;
    }
    std::vector<int> oldPath[2];
    std::vector<int> ind[2];
    if (!Path(g, s, t, oldPath[0], ind[0])) {
        return false;
    }
    if (!Path(g, s, t, oldPath[1], ind[1])) {
        return false;
    }
    // std::cout << oldPath[0] << oldPath[1];
    int cur = 0;
    path0.push_back(oldPath[0][0] + 1);
    for (std::vector<int>::iterator it0 = oldPath[0].begin() + 1;
        it0 != oldPath[0].end() - 1 && it0 != oldPath[1].end() - 1; ++it0)
    {
        if (ind[!cur][*it0] != -1) {
            cur = !cur;
            it0 = oldPath[cur].begin() + ind[cur][*it0];
        }
        path0.push_back(*it0 + 1);
    }
    cur = 1;
    path1.push_back(oldPath[1][0] + 1);
    for (std::vector<int>::iterator it1 = oldPath[1].begin() + 1;
        it1 != oldPath[0].end() - 1 && it1 != oldPath[1].end() - 1; ++it1)
    {
        if (ind[!cur][*it1] != -1) {
            cur = !cur;
            it1 = oldPath[cur].begin() + ind[cur][*it1];
        }
        path1.push_back(*it1 + 1);
    }
    path0.push_back(oldPath[0].back() + 1);
    path1.push_back(oldPath[1].back() + 1);
    return true;
}

/*
int main()
{
    int n = 0;
    int m = 0;
    int s = 0;
    int t = 0;
    std::cin >> n >> m >> s >> t;
    std::vector<std::multiset<int>> g(n);
    for (int i = 0; i < m; ++i) {
        int from = 0;
        int to = 0;
        std::cin >> from >> to;
        g[--from].insert(--to);
    }
    std::vector<int> path1;
    std::vector<int> path2;
    if (Flow(g, --s, --t, path1, path2)) {
        std::cout << "YES\n" << path1 << path2;
    } else {
        std::cout << "NO\n";
    }
    return 0;
}
*/

/*
10 13 1 6
1 7
7 2
1 2
2 3
3 9
9 6
3 4
4 5
5 6
5 10
10 6
1 8
8 4
YES
1 2 3 4 5 6 
1 9 6 
*/