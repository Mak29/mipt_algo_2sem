#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "E.h"

TEST_CASE("Test 0")
{
    std::vector<std::multiset<int>> g(2);
    g[0].insert(0);
    g[1].insert(1);
    std::vector<int> path1;
    std::vector<int> path2;
    REQUIRE(!Flow(g, 0, 1, path1, path2));
}

TEST_CASE("Test 1")
{
    std::vector<std::multiset<int>> g(3);
    g[0].insert(1);
    g[0].insert(2);
    g[1].insert(2);
    g[1].insert(0);
    g[2].insert(0);
    g[2].insert(1);
    std::vector<int> path1;
    std::vector<int> path2;
    REQUIRE(Flow(g, 0, 1, path1, path2));
}

TEST_CASE("Test 2")
{
    std::vector<std::multiset<int>> g(10);
    g[1 - 1].insert(7 - 1);
    g[7 - 1].insert(1 - 1);
    g[7 - 1].insert(2 - 1);
    g[2 - 1].insert(7 - 1);
    g[1 - 1].insert(2 - 1);
    g[2 - 1].insert(1 - 1);
    g[2 - 1].insert(3 - 1);
    g[3 - 1].insert(2 - 1);
    g[3 - 1].insert(9 - 1);
    g[9 - 1].insert(3 - 1);
    g[9 - 1].insert(6 - 1);
    g[6 - 1].insert(9 - 1);
    g[3 - 1].insert(4 - 1);
    g[4 - 1].insert(3 - 1);
    g[4 - 1].insert(5 - 1);
    g[5 - 1].insert(4 - 1);
    g[5 - 1].insert(6 - 1);
    g[6 - 1].insert(5 - 1);
    g[5 - 1].insert(10 - 1);
    g[10 - 1].insert(5 - 1);
    g[10 - 1].insert(6 - 1);
    g[6 - 1].insert(10 - 1);
    g[1 - 1].insert(8 - 1);
    g[8 - 1].insert(1 - 1);
    g[8 - 1].insert(4 - 1);
    g[4 - 1].insert(8 - 1);
    std::vector<int> path1;
    std::vector<int> path2;
    REQUIRE(Flow(g, 0, 5, path1, path2));
}