#include <algorithm>
#include <iostream>
#include <vector>

struct CEdge{
    int from;
    int to;
    unsigned long long weight;
};

bool operator< (const CEdge& u, const CEdge& v)
{
    return u.weight < v.weight;
}

class CDsu{
public:
    CDsu(int n) : parent(n), size(n, 1) {
        for (int i = 0; i < n; ++i) {
            parent[i] = i;
        }
    }

    int get(int i)
    {
        if (i == parent[i]) {
            return i;
        }
        return parent[i] = get(parent[i]);
    }

    void unite(int i, int j)
    {
        i = get(i);
        j = get(j);
        if (size[i] < size[j]) {
            std::swap(i, j);
        }
        parent[j] = i;
        size[i] += size[j];
    }
private:
    std::vector<int> parent;
    std::vector<int> size;
};

unsigned long long Kruskal(int numVertices, std::vector<CEdge>& edges)
{
    std::sort(edges.begin(), edges.end());
    CDsu dsu(numVertices);
    unsigned long long sum = 0;
    for (auto edge : edges) {
        if (dsu.get(edge.from) != dsu.get(edge.to)) {
            dsu.unite(edge.from, edge.to);
            sum += edge.weight;
        }
    }
    return sum;
}

/*
int main()
{
    int numVertices = 0;
    int numEdges = 0;
    std::cin >> numVertices >> numEdges;
    std::vector<CEdge> edges;
    std::vector<unsigned long long> a(numVertices);
    std::cin >> a[0];
    int imin = 0;
    for (int i = 1; i < numVertices; ++i) {
        std::cin >> a[i];
        if (a[i] < a[imin]) {
            imin = i;
        }
    }

    for (int i = 0; i < numVertices; ++i) {
        if (i != imin) {
            edges.push_back({imin, i, a[imin] + a[i]});
        }
    }
    for (int i = 0; i < numEdges; ++i) {
        int from = 0;
        int to = 0;
        unsigned long long weight = 0;
        std::cin >> from >> to >> weight;
        edges.push_back({--from, --to, weight});
    }
    std::cout << Kruskal(numVertices, edges) << std::endl;
    return 0;
}
*/