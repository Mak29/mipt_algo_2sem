#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "C.h"

TEST_CASE("Test 1")
{
    int n = 3;
    std::vector<CEdge> edges;
    edges.push_back({0, 1, 4});
    edges.push_back({0, 2, 4});
    edges.push_back({1, 2, 5});
    edges.push_back({1, 0, 1});
    REQUIRE(Kruskal(n, edges) == 5);
}

TEST_CASE("Test 2")
{
    int n = 4;
    std::vector<CEdge> edges;
    edges.push_back({0, 1, 4});
    edges.push_back({0, 2, 4});
    edges.push_back({0, 3, 8});
    REQUIRE(Kruskal(n, edges) == 16);
}

TEST_CASE("Test 3")
{
    int n = 5;
    std::vector<CEdge> edges;
    edges.push_back({0, 1, 3});
    edges.push_back({0, 2, 4});
    edges.push_back({0, 3, 5});
    edges.push_back({0, 4, 6});
    edges.push_back({1 - 1, 2 - 1, 8});
    edges.push_back({1 - 1, 3 - 1, 10});
    edges.push_back({1 - 1, 4 - 1, 7});
    edges.push_back({1 - 1, 5 - 1, 15});
    REQUIRE(Kruskal(n, edges) == 18);
}