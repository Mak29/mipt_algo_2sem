#include <ctime>
#include <iostream>
#include <vector>

struct CNode{
    std::string value = "";
    int size = 0;
    int priority = 0;
    CNode* left = nullptr;
    CNode* right = nullptr;
    CNode* parent = nullptr;

    CNode() = default;
    CNode(std::string value);
};

CNode::CNode(std::string value)
{
    this->value = value;
    size = 1;
    priority = std::rand();
}

int Size(CNode* p) {
    if (p == nullptr) {
        return 0;
    }
    return p->size;
}

class CTreap{
public:
    CTreap() = default;
    void insert(int pos, const std::string& value);
    void erase(int pos);
    std::string find(int pos);
private:
    CNode* root = nullptr;

    CNode* findPtr(int pos, CNode* current);

    std::pair<CNode*, CNode*> split(CNode* current, int key);
    CNode* merge(CNode* one, CNode* two);
};

CNode* CTreap::findPtr(int pos, CNode* current)
{
    if (pos < Size(current->left)) {
        return findPtr(pos, current->left);
    }
    if (pos > Size(current->left)) {
        return findPtr(pos - Size(current->left) - 1, current->right);
    }
    return current;
}

std::pair<CNode*, CNode*> CTreap::split(CNode* current, int key)
{
    if (current == nullptr) {
        return {nullptr, nullptr};
    }
    if (key <= Size(current->left)) {
        std::pair<CNode*, CNode*> div = split(current->left, key);
        current->left = div.second;
        if (div.second != nullptr) {
            div.second->parent = current;
        }
        current->size = Size(current->left) + Size(current->right) + 1;
        if (div.first != nullptr) {
            div.first->parent = nullptr;
        }
        return {div.first, current};
    } else {
        std::pair<CNode*, CNode*> div = split(current->right, key - Size(current->left) - 1);
        current->right = div.first;
        if (div.first != nullptr) {
            div.first->parent = current;
        }
        current->size = Size(current->left) + Size(current->right) + 1;
        if (div.second != nullptr) {
            div.second->parent = nullptr;
        }
        return {current, div.second};
    }

}
CNode* CTreap::merge(CNode* one, CNode* two)
{
    if (one == nullptr) {
        return two;
    }
    if (two == nullptr) {
        return one;
    }
    if (one->priority > two->priority) {
        one->right = merge(one->right, two);
        one->right->parent = one;
        one->size += two->size;
        return one;
    } else {
        two->left = merge(one, two->left);
        two->left->parent = two;
        two->size += one->size;
        return two;
    }
}

void CTreap::insert(int pos, const std::string& value)
{
    std::pair<CNode*, CNode*> div = split(root, pos);
    root = merge(merge(div.first, new CNode(value)), div.second);
}
void CTreap::erase(int pos)
{
    std::pair<CNode*, CNode*> l = split(root, pos);
    std::pair<CNode*, CNode*> r = split(l.second, 1);
    root = merge(l.first, r.second);
    delete r.first;
}
std::string CTreap::find(int pos)
{
    return findPtr(pos, root)->value;
}
/*
int main()
{
    CTreap t;
    std::string type = "";
    int pos = 0;
    int n = 0;
    std::cin >> n;
    for (int i = 0; i < n; ++i) {
        std::cin >> type >> pos;
        if (type == "?") {
            std::cout << t.find(pos) << std::endl;
        } else if (type == "+") {
            std::string str;
            std::cin >> str;
            t.insert(pos, str);
        } else if (type == "-") {
            int endpos = 0;
            std::cin >> endpos;
            for (int i = endpos; i >= pos; --i) {
                t.erase(i);
            }
        }
    }
    return 0;
}
*/