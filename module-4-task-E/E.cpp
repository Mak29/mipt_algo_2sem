#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "E.h"

TEST_CASE("Test 0")
{
    CTreap t;
    t.insert(0, "Hello world!");
    REQUIRE(t.find(0) == "Hello world!");
    t.erase(0);
    REQUIRE(true);
}

TEST_CASE("Test 1")
{
    CTreap t;
    t.insert(0, "myau");
    t.insert(0, "krya");
    REQUIRE(t.find(0) == "krya");
    t.insert(2, "gav");
    t.erase(1);
    REQUIRE(t.find(1) == "gav");
}

TEST_CASE("Test 2")
{
    CTreap t;
    t.insert(0, "a");
    t.insert(1, "b");
    t.insert(1, "c");
    t.insert(2, "d");
    t.erase(0);
    REQUIRE(t.find(0) == "c");
    t.insert(3, "e");
    t.insert(4, "f");
    t.insert(1, "g");
    REQUIRE(t.find(0) == "c");
    REQUIRE(t.find(1) == "g");
}