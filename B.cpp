// Топологическая сортировка
#include <iostream>
#include <vector>

enum TVerticesColorType {
	VCT_WHITE,
	VCT_GREY,
	VCT_BLACK
};

void dfs( int v, std::vector<int>& color, const std::vector< std::vector<int> >& graph,
	std::vector<int>& traversal )
{
	static int out = 0; // переменная, в которой храним, из скольких вершин мы уже вышли 
	// в течение обхода в глубину
	color[v] = VCT_GREY;
	for( int i = 0; i < graph[v].size(); ++i ) {
		if( color[graph[v][i]] == VCT_GREY ) { // проверка на наличие цикла
			// наличие цикла равносильно невозможности сделать топологическую сортировку
			std::cout << "NO"; 
			exit( 0 );
		}
		if( color[graph[v][i]] == VCT_WHITE ) {
			dfs( graph[v][i], color, graph, traversal );
		}
	}
	traversal[graph.size() - ++out] = v; 
	color[v] = VCT_BLACK;
}

void topSort( const std::vector< std::vector<int> >& graph, std::vector<int>& traversal )
{
	std::vector<int> color( graph.size(), 0 );
	for( int i = 0; i < graph.size(); ++i ) {
		if( !color[i] ) {
			dfs( i, color, graph, traversal );
		}
	}
}

int main()
{
	int numVertices = 0;
	int numEdges = 0;
	std::cin >> numVertices >> numEdges;
	std::vector< std::vector<int> > graph( numVertices );
	for( int i = 0; i < numEdges; ++i ) {
		int x = 0, y = 0;
		std::cin >> x >> y;
		graph[x].push_back( y );
	}

	// массив, в котором записаны вершины в порядке обхода
	std::vector<int> traversal( numVertices, 0 );
	
	topSort( graph, traversal ); // пытаемся сделать топологическую сортировку
	std::cout << "YES\n"; // если смогли её сделать
	for( int i = 0; i < numVertices; ++i ) {
		std::cout << traversal[i] << ' ';
	}
	return 0;
}