#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "A.h"

TEST_CASE("Test 0")
{
    std::vector<int> a;
    a.push_back(0);
    a.push_back(0);
    CSparseTable t(a);
    REQUIRE(t.findMin(0, 1) == 0);
}

TEST_CASE("Test 1")
{
    std::vector<int> a;
    for (int i = 1; i <= 10; ++i) {
        a.push_back(i);
    }
    CSparseTable t(a);
    REQUIRE(t.findMin(1 - 1, 2 - 1) == 2);
    REQUIRE(t.findMin(1 - 1, 10 - 1) == 2);
    REQUIRE(t.findMin(2 - 1, 7 - 1) == 3);
}

TEST_CASE("Test 2")
{
    std::vector<int> a;
    a.push_back(2);
    a.push_back(3);
    a.push_back(6);
    a.push_back(2);
    a.push_back(4);
    a.push_back(1);
    a.push_back(5);
    a.push_back(5);
    a.push_back(6);
    a.push_back(8);
    CSparseTable t(a);
    REQUIRE(t.findMin(0, 9) == 2);
}