#include <iostream>
#include <vector>

struct CInt{
    int value;
    int index;
};

CInt Min(CInt a, CInt b)
{
    return (a.value < b.value ? a : b);
}
template<typename T, typename... args>
T Min(T head, args... tail)
{
    return Min(head, Min(tail...));
}
CInt Max(CInt a, CInt b)
{
    return (a.value < b.value ? b : a);
}
std::pair<CInt, CInt> Min(std::pair<CInt, CInt> a, std::pair<CInt, CInt> b)
{
    std::pair<CInt, CInt> c;
    c.first = Min(a.first, b.first);
    c.second = (a.first.index == b.first.index ? Min(a.second, b.second) : Min(Max(a.first, b.first), a.second, b.second));
    return c;
}

class CSparseTable{
public:
    CSparseTable(const std::vector<int>& a);
    int findMin(int l, int r) const;
private:
    std::vector<std::vector<std::pair<CInt, CInt>>> table;
    std::vector<int> log;
    void countLog(int n);
};
CSparseTable::CSparseTable(const std::vector<int>& a)
{
    countLog(a.size());
    table.resize(log.back() + 1);
    for (int i = 0; i < a.size() - 1; ++i) {
        CInt u = CInt{a[i], i};
        CInt v = CInt{a[i + 1], i + 1};
        table[1].push_back({Min(u, v), Max(u, v)});
    }
    for (int k = 2; k < table.size(); ++k) {
        for (int i = 0; i + (1 << (k - 1)) < table[k - 1].size(); ++i) {
            table[k].push_back(Min(table[k - 1][i], table[k - 1][i + (1 << (k - 1))]));
        }
    }
}
int CSparseTable::findMin(int l, int r) const
{
    int k = log[r - l + 1];
    return Min(table[k][l], table[k][r - (1 << k) + 1]).second.value;
}
void CSparseTable::countLog(int n)
{
    CSparseTable::log.resize(n + 1);
    for (int i = 1, k = 0; i <= n; ++k) {
        for (; i <= n && (1 << (k + 1)) > i; ++i) {
            CSparseTable::log[i] = k;
        }
    }
}

/*
int main()
{
    int n = 0;
    int m = 0;
    std::cin >> n >> m;
    std::vector<int> a(n);
    for (int i = 0; i < n; ++i) {
        std::cin >> a[i];
    }

    CSparseTable t(a);
    for (int i = 0; i < m; ++i) {
        int l = 0;
        int r = 0;
        std::cin >> l >> r;
        std::cout << t.findMin(--l, --r) << std::endl;
    }
    return 0;
}
*/