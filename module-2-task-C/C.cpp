#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "C.h"

TEST_CASE("Test 0")
{
    std::vector<double> empty(3, -1.);
    std::vector<std::vector<double>> g(3, empty);
    REQUIRE(abs(FindWay(g, 1, 1) - 1.) < 1e-15);
}

TEST_CASE("Test 1")
{
    std::vector<double> empty(5, -1.);
    std::vector<std::vector<double>> g(5, empty);
    g[1 - 1][2 - 1] = 1. - 0.20;
    g[3 - 1][4 - 1] = 1. - 0.10;
    g[1 - 1][5 - 1] = 1. - 0.37;
    g[1 - 1][3 - 1] = 1. - 0.50;
    g[2 - 1][3 - 1] = 1. - 0.20;
    g[4 - 1][5 - 1] = 1. - 0.92;
    g[1 - 1][4 - 1] = 1. - 0.67;
    REQUIRE(abs(FindWay(g, 1 - 1, 3 - 1) - (1. - 0.36)) < 1e-15);
}

TEST_CASE("Test 2")
{
    std::vector<double> empty(4, -1.);
    std::vector<std::vector<double>> g(4, empty);
    g[0][1] = 0.9;
    g[2][3] = 0.5;
    g[0][2] = 0.2;
    g[1][3] = 0.11;
    REQUIRE(abs(FindWay(g, 0, 3) - 0.099) < 1e-15);
}
