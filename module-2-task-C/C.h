#include <climits>
#include <iostream>
#include <queue>
#include <vector>

struct CVertex
{
    int number = 0;
    double dist = 0;

    CVertex(int number = 0, double dist = 0.) : number(number), dist(dist)
    {
    }
};

bool operator<(const CVertex &u, const CVertex &v)
{
    if (u.dist == v.dist) {
        return (u.number < v.number);
    }
    return (u.dist < v.dist);
}

void TryUpdate(int who, std::priority_queue<CVertex>& where, std::vector<double>& dist, double newDist)
{
    if (newDist <= dist[who]) {
        return;
    }
    dist[who] = newDist;
    where.push({who, newDist});
}

double FindWay(std::vector<std::vector<double>> g, int from, int to)
{
    std::vector<double> dist(g.size(), 0.);
    std::vector<bool> is(g.size(), false);
    std::priority_queue<CVertex> undiscovered;
    undiscovered.push({from, 1.});
    dist[from] = 1.;
    for (int count = 0; count < g.size(); ++count) {
        CVertex newVertex = undiscovered.top();
        undiscovered.pop();
        while (is[newVertex.number]) {
            newVertex = undiscovered.top();
            undiscovered.pop();
        }
        is[newVertex.number] = true;
        if (newVertex.number == to) {
            return newVertex.dist;
        }
        for (int i = 0; i < g.size(); ++i) {
            if (g[newVertex.number][i] != -1.) {
                TryUpdate(i, undiscovered, dist, newVertex.dist * g[newVertex.number][i]);
            }
        }
    }
    return dist[to];
}

