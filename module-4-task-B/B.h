
#include <iostream>
#include <vector>

class CSegmentsTree{
public:
    CSegmentsTree(const std::vector<int>& array);
    int max(int l, int r);
    void add(int l, int r, int value);
private:
    std::vector<int> maxValue;
    std::vector<int> addValue;

    void push(int v);
    int left(int v) const;
    int right(int v) const;

    int maxImpl(int v, int l, int r, int tl, int tr);
    void addImpl(int v, int l, int r, int tl, int tr, int value);
};

int Log(int n)
{
    int k = 0;
    for (; n > 0; n >>= 1) {
        ++k;
    }
    return k - 1;
}

int CSegmentsTree::left(int v) const
{
    return 2 * v;
}
int CSegmentsTree::right(int v) const
{
    return 2 * v + 1;
}
void CSegmentsTree::push(int v)
{
    addValue[left(v)] += addValue[v];
    addValue[right(v)] += addValue[v];
    maxValue[left(v)] += addValue[v];
    maxValue[right(v)] += addValue[v];
    addValue[v] = 0;
}

CSegmentsTree::CSegmentsTree(const std::vector<int>& array)
{
    int n = (1 << (Log(array.size() - 1) + 1));
    //std::cout << n << std::endl;
    maxValue.resize(2 * n);
    addValue.resize(2 * n, 0);
    for (int i = 0; i < array.size(); ++i) {
        maxValue[n + i] = array[i];
    }
    for (int i = n - 1; i > 0; --i) {
        maxValue[i] = std::max(maxValue[left(i)], maxValue[right(i)]);
    }
    //std::cout << maxValue << addValue;
}

int CSegmentsTree::max(int l, int r)
{
    //std::cout << "max:\n";
    int ans = maxImpl(1, l, r, 0, maxValue.size() / 2 - 1);
    //std::cout << ans << std::endl << maxValue << addValue;
    return ans; 
}
void CSegmentsTree::add(int l, int r, int value)
{
    //std::cout << "add:\n";
    addImpl(1, l, r, 0, addValue.size() / 2 - 1, value);
    //std::cout << maxValue << addValue;
}

int CSegmentsTree::maxImpl(int v, int l, int r, int tl, int tr)
{
    if (l == tl && r == tr) {
        return maxValue[v];
    }
    int ans = 0;
    int tm = (tl + tr) / 2;
    push(v);
    if (l <= tm) {
        ans = maxImpl(left(v), l, std::min(r, tm), tl, tm);
    }
    if (r > tm) {
        ans = std::max(ans, maxImpl(right(v), std::max(l, tm + 1), r, tm + 1, tr));
    }
    return ans;
}
void CSegmentsTree::addImpl(int v, int l, int r, int tl, int tr, int value)
{
    if (l == tl && r == tr) {
        addValue[v] += value;
        maxValue[v] += value;
        return;
    }
    int tm = (tl + tr) / 2;
    push(v);
    if (l <= tm) {
        addImpl(left(v), l, std::min(r, tm), tl, tm, value);
    }
    if (r > tm) {
        addImpl(right(v), std::max(l, tm + 1), r, tm + 1, tr, value);
    }
    maxValue[v] = std::max(maxValue[left(v)], maxValue[right(v)]);
}
/*
int main()
{
    int n = 0;
    std::cin >> n;
    --n;
    std::vector<int> a(n);
    for (int i = 0; i < n; ++i) {
        std::cin >> a[i];
    }
    CSegmentsTree t(a);
    int capacity = 0;
    std::cin >> capacity;
    int m = 0;
    std::cin >> m;
    for (int i = 0; i < m; ++i) {
        int l = 0;
        int r = 0;
        int value = 0;
        std::cin >> l >> r >> value;
        //std::cout << l << ' ' << r << ' ' << value << std::endl;
        --r;
        if (t.max(l, r) + value > capacity) {
            std::cout << i << ' ';
        } else {
            t.add(l, r, value);
        }
        //std::cout << "-------------------\n";
    }
    return 0;
}
*/
