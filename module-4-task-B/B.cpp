#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "B.h"

TEST_CASE("Test 0")
{
    CSegmentsTree t({0});
    REQUIRE(t.max(0, 0) == 0);
}

TEST_CASE("Test 1")
{
    std::vector<int> a;
    for (int i = 1; i < 10; ++i) {
        a.push_back(i);
    }
    a.push_back(0);
    CSegmentsTree t(a);
    REQUIRE(t.max(9, 10) + 1 <= 15);
}

TEST_CASE("Test 2")
{
    std::vector<int> a;
    a.push_back(1); 
    a.push_back(0); 
    a.push_back(0); 
    a.push_back(0); 
    a.push_back(1);
    CSegmentsTree t(a);
    int capacity = 5;
    if (t.max(0, 2 - 1) + 1 > capacity) {
        REQUIRE(false);
    } else {
        t.add(0, 2 - 1, 1);
    }
    if (t.max(1, 4 - 1) + 2 > capacity) {
        REQUIRE(false);
    } else {
        t.add(1, 4 - 1, 2);
    }
    if (t.max(2, 5 - 1) + 1 > capacity) {
        REQUIRE(false);
    } else {
        t.add(2, 5 - 1, 1);
    }
    if (t.max(1, 3 - 1) + 3 > capacity) {
        REQUIRE(true);
    } else {
        t.add(1, 3 - 1, 3);
    }
    if (t.max(3, 5 - 1) + 2 > capacity) {
        REQUIRE(false);
    } else {
        t.add(3, 5 - 1, 2);
    }
    if (t.max(0, 4 - 1) + 1 > capacity) {
        REQUIRE(true);
    } else {
        t.add(0, 4 - 1, 1);
    }
    if (t.max(0, 1 - 1) + 3 > capacity) {
        REQUIRE(false);
    } else {
        t.add(0, 1 - 1, 3);
    }
    if (t.max(1, 3 - 1) + 2 > capacity) {
        REQUIRE(false);
    } else {
        t.add(1, 3 - 1, 2);
    }
}