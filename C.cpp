#include <algorithm>
#include <iostream>
#include <vector>

class CGraph {
public:
	CGraph( int initNumVertices )
	{
		numVertices = initNumVertices;
		edge.resize( numVertices );
		revEdge.resize( numVertices );
		traversal.resize( numVertices );
		scc.resize( numVertices );
	}
	~CGraph()
	{
	}

	void PushEdge( int x, int y )
	{
		edge[x].push_back( y );
		revEdge[y].push_back( x );
	}
	int CountOfEdgesToStrongCohesion()
	{
		// сначала алгоритм поиска КСС (Косарайю)

		// записываем вершины в порядке времён выхода в обходе инвертированного графа
		sortVertexesByOut();

		openedVertices.resize( numVertices );
		std::vector<int> empty;
		// обход в полученном порядке в изначальном графе
		for( int i = 0, numberOfSCC = 0; i < numVertices; ++i ) {
			if( !openedVertices[traversal[i]] ) {
				listSCC.push_back( empty );
				dfsBySCC( traversal[i] , numberOfSCC++ );
			}
		}
		openedVertices.clear();

		if( singleComponent() ) {
			return 0;
		}

		// Считаем количество корней и листьев в графе, где
		// вершины - это КСС, а рёбра - рёбра между КСС
		std::pair<int, int> count = countLeafsAndRoots();
		return std::max( count.first, count.second ); // ответ максимум - из количеств
	}
	
private:
	int numVertices;
	// список смежности графа
	std::vector< std::vector<int> > edge;

	// список смежности инвертированного графа
	std::vector< std::vector<int> > revEdge;

	// массив компонент связности
	std::vector< std::vector<int> > listSCC;

	// порядок обхода инвертированного графа
	std::vector<int> traversal;

	// массив номеров компонент связности для каждой вершины
	std::vector<int> scc;
	
	// массив пройденных вершин
	std::vector<bool> openedVertices;

	
 	// сколько вершин уже обошли во время обхода
	int out = 0;

	bool singleComponent()
	{
		return listSCC.size() == 1;
	}

	bool isEdgeToOtherSCC( const std::vector< std::vector<int> >& edge,
		int numOfSCC, int numOfVertexInSCC, int numOfNeighbour, int currentSCC )
	{
		return scc[edge[ listSCC[numOfSCC][numOfVertexInSCC] ][ numOfNeighbour ]] != currentSCC;
	}
	bool isLeaf( const std::vector< std::vector<int> >& edge, int numOfSCC )
	{ // проверка, является ли КСС листом
		bool isLeaf = true;
		int currentSCC = scc[listSCC[numOfSCC][0]];
		for( int i = 0; i < listSCC[numOfSCC].size(); ++i ) {
			// обходим все вершины КСС
			for( int j = 0; j < edge[listSCC[numOfSCC][i]].size(); ++j ) {
				// обходим всех соседей вершины КСС
				if( isEdgeToOtherSCC( edge, numOfSCC, i, j, currentSCC ) ) {
					// проверяем, есть ли ребро в другую КСС
					isLeaf = false;
				}
			}
		}
		return isLeaf;
	}
	std::pair<int, int> countLeafsAndRoots()
	{ // подсчёт количества листьев и корней в графе КСС
		int countOfLeafs = 0;
		int countOfRoots = 0;
		for( int i = 0; i < listSCC.size(); ++i ) {
			countOfLeafs += isLeaf( edge, i );
			countOfRoots += isLeaf( revEdge, i ); // корень - лист в инвертированном графе
		}
		return { countOfLeafs, countOfRoots };
	}
	void sortDfs( int v )
	{ // обход в глубину, в котором сортируем вершины по времени выхода
		openedVertices[v] = true;
		for( int i = 0; i < revEdge[v].size(); ++i ) {
			if( openedVertices[revEdge[v][i]] == 0 ) {
				sortDfs( revEdge[v][i] );
			}
		}
		traversal[numVertices - ++out] = v;
	}
	void sortVertexesByOut()
	{ // обходим все вершины и вызываем обход в глубину для непосещённых
		openedVertices.resize( numVertices );
		for( int i = 0; i < numVertices; ++i ) {
			if( !openedVertices[i] ) {
				sortDfs( i );
			}
		}
		openedVertices.clear();
	}
	void dfsBySCC( int v, const int& numberOfSCC )
	{ // обход глубину, в котором вершины одной КСС составляют дерево обхода
		openedVertices[v] = true;
		scc[v] = numberOfSCC; 
		listSCC.back().push_back( v );
		for( int i = 0; i < edge[v].size(); ++i ) {
			if( !openedVertices[edge[v][i]] ) {
				dfsBySCC( edge[v][i], numberOfSCC );
			}
		}
	}
};

int main()
{
	int numVertices = 0;
	int numEdges = 0;
	std::cin >> numVertices >> numEdges;
	CGraph g( numVertices );
	for( int i = 0; i < numEdges; ++i ) {
		int x = 0;
		int y = 0;
		std::cin >> x >> y;
		--x;
		--y;
		g.PushEdge( x, y );
	}
	std::cout << g.CountOfEdgesToStrongCohesion();
	return 0;
}