#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "C.h"

TEST_CASE("Test 0")
{
    CSegmentsTree t({0});
    t.set(0, 0, 1);
    REQUIRE(t.min(0, 0) == 1);
}

TEST_CASE("Test 1")
{
    std::vector<int> a;
    a.push_back(7 + 40 + 3);
    a.push_back(54 + 90 + 255);
    a.push_back(44 + 230 + 8);
    a.push_back(33 + 57 + 132);
    a.push_back(17 + 8 + 5);
    CSegmentsTree t(a);
    t.set(0, 3, 100 + 40 + 41);
    REQUIRE(t.min(2, 4) == 30);
}

TEST_CASE("Test 2")
{
    std::vector<int> a;
    a.push_back(7 + 40 + 3);
    a.push_back(54 + 90 + 255);
    a.push_back(44 + 230 + 8);
    a.push_back(33 + 57 + 132);
    a.push_back(17 + 8 + 5);
    CSegmentsTree t(a);
    t.set(0, 3, 100 + 40 + 41);
    t.set(2, 4, 0 + 200 + 57);
    REQUIRE(t.min(1, 3) == 181);
}