#include <iostream>
#include <vector>

class CSegmentsTree{
public:
    CSegmentsTree(const std::vector<int>& array);
    int min(int l, int r);
    void set(int l, int r, int value);
private:
    std::vector<int> setValue;
    std::vector<int> minValue;

    void push(int v);
    int left(int v) const;
    int right(int v) const;

    int minImpl(int v, int l, int r, int tl, int tr);
    void setImpl(int v, int l, int r, int tl, int tr, int value);
};

int Log(int n)
{
    int k = 0;
    for (; n > 0; n >>= 1) {
        ++k;
    }
    return k - 1;
}

int CSegmentsTree::left(int v) const
{
    return 2 * v;
}
int CSegmentsTree::right(int v) const
{
    return 2 * v + 1;
}
void CSegmentsTree::push(int v)
{
    if (setValue[v] != 800) {
        setValue[left(v)] = setValue[v];
        setValue[right(v)] = setValue[v];
        minValue[left(v)] = setValue[v];
        minValue[right(v)] = setValue[v];
        setValue[v] = 800;
    }
}

CSegmentsTree::CSegmentsTree(const std::vector<int>& array)
{
    int n = (1 << (Log(array.size() - 1) + 1));
    minValue.resize(2 * n);
    setValue.resize(2 * n, 0);
    for (int i = 0; i < array.size(); ++i) {
        setValue[n + i] = array[i];
        minValue[n + i] = array[i];
    }
    for (int i = n - 1; i > 0; --i) {
        minValue[i] = std::min(minValue[left(i)], minValue[right(i)]);
        setValue[i] = 800;
    }
}

int CSegmentsTree::min(int l, int r)
{
    int ans = minImpl(1, l, r, 0, setValue.size() / 2 - 1);
    return ans; 
}
void CSegmentsTree::set(int l, int r, int value)
{
    setImpl(1, l, r, 0, setValue.size() / 2 - 1, value);
}

int CSegmentsTree::minImpl(int v, int l, int r, int tl, int tr)
{
    if (l == tl && r == tr) {
        return minValue[v];
    }
    int ans = 800;
    int tm = (tl + tr) / 2;
    push(v);
    if (l <= tm) {
        ans = minImpl(left(v), l, std::min(r, tm), tl, tm);
    }
    if (r > tm) {
        ans = std::min(ans, minImpl(right(v), std::max(l, tm + 1), r, tm + 1, tr));
    }
    return ans;
}
void CSegmentsTree::setImpl(int v, int l, int r, int tl, int tr, int value)
{
    if (l == tl && r == tr) {
        setValue[v] = value;
        minValue[v] = value;
        return;
    }
    int tm = (tl + tr) / 2;
    push(v);
    if (l <= tm) {
        setImpl(left(v), l, std::min(r, tm), tl, tm, value);
    }
    if (r > tm) {
        setImpl(right(v), std::max(l, tm + 1), r, tm + 1, tr, value);
    }
    minValue[v] = std::min(minValue[left(v)], minValue[right(v)]);
}
/*
int main()
{
    int n = 0;
    std::cin >> n;
    std::vector<int> a;
    for (int i = 0; i < n; ++i) {
        int r = 0;
        int g = 0;
        int b = 0;
        std::cin >> r >> g >> b;
        a.push_back(r + g + b);
    }
    CSegmentsTree t(a);
    int m = 0;
    std::cin >> m;
    for (int i = 0; i < m; ++i) {
        int c = 0;
        int d = 0;
        int r = 0;
        int g = 0;
        int b = 0;
        int e = 0;
        int f = 0;
        std::cin >> c >> d >> r >> g >> b >> e >> f;
        t.set(c, d, r + g + b);
        std::cout << t.min(e, f) << ' ';
    }
    return 0;
}
*/